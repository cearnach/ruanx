package com.ruanx.netease.bean;

/**
 * <p>标题: </p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2019</p>
 * <p>公司: 智业软件股份有限公司</p>
 *
 * @version: 1.0
 * @author: ruansheng
 * @date 5/16/2019
 */
public class BloomFilterConstrain {

    public static final String ALL_MUSIC_ID = "ALL_MUSIC_ID_BLOOM_FILTER";

    public static final String EXCLUDE_MUSIC_ID = "EXCLUDE_MUSIC_ID_BLOOM_FILTER";
}
