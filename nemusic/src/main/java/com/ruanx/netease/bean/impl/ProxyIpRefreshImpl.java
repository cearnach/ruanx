package com.ruanx.netease.bean.impl;

import com.ruanx.netease.bean.GlobalProxy;
import com.ruanx.netease.bean.ProxyIpRefresh;
import com.ruanx.netease.property.CrawlerProperties;
import com.ruanx.netease.util.JsoupUtil;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>标题: </p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2019</p>
 * <p>公司: 智业软件股份有限公司</p>
 *
 * @version: 1.0
 * @author: ruansheng
 * @date 5/14/2019
 */
@Component
@Slf4j
public class ProxyIpRefreshImpl implements ProxyIpRefresh {

    private final CrawlerProperties crawlerProperties;

    public ProxyIpRefreshImpl(CrawlerProperties crawlerProperties) {
        this.crawlerProperties = crawlerProperties;
    }

    private Map<String, String> obtainProxy() throws IOException {
        log.info("开始获取IP代理");
        String jsonStr =
                JsoupUtil.connectIgnoreProxy(crawlerProperties.getProxyFetchUrl()).get().body().text();
        ProxyBean proxyBean = JSONObject.parseObject(jsonStr, ProxyBean.class);
        List<ProxyBean.DataBean> ipList = proxyBean.getData();
        if (CollectionUtils.isEmpty(ipList)) {
            log.error("IP代理列表为空 , 详情: {}", jsonStr);
            return null;
        }
        ProxyBean.DataBean dataBean = ipList.get(0);
        String ip = dataBean.getIp();
        String port = dataBean.getPort();
        log.info("获取IP代理成功 , 地址 : {}:{}", ip, port);
        Map<String, String> map = new HashMap<>(2);
        map.put("ip", dataBean.getIp());
        map.put("port", dataBean.getPort());
        return map;
    }

    @Override
    public synchronized void refreshGlobalProxy() throws IOException, InterruptedException {
        Map<String, String> ipMap = this.obtainProxy();
        if (ipMap == null) {
            return;
        }
        GlobalProxy.HOST = ipMap.get("ip");
        GlobalProxy.PORT = Integer.valueOf(ipMap.get("port"));
        log.info("设置全局代理成功");
        Thread.sleep(2000);
    }

    @Override
    public void reset() {
        GlobalProxy.HOST = "";
    }
}
