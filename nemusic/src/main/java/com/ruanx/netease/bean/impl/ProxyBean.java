package com.ruanx.netease.bean.impl;

import lombok.Data;

import java.util.List;

/**
 * <p>标题: </p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2019</p>
 * <p>公司: 智业软件股份有限公司</p>
 *
 * @version: 1.0
 * @author: ruansheng
 * @date 5/14/2019
 */
@Data
public class ProxyBean {

    /**
     * code : 0
     * success : true
     * msg : 0
     * data : [{"ip":"115.210.37.29","port":"43311","expire_time":"2019-05-14 12:22:08"}]
     */
    private int code;
    private boolean success;
    private String msg;
    private List<DataBean> data;

    @Data
    public static class DataBean {
        /**
         * ip : 115.210.37.29
         * port : 43311
         * expire_time : 2019-05-14 12:22:08
         */
        private String ip;
        private String port;
        private String expire_time;

    }
}
