package com.ruanx.netease.bean;

import com.ruanx.netease.enumeration.IgnoreTypeEnum;
import com.ruanx.netease.service.IgnoreMusicItemService;
import com.ruanx.netease.service.MusicService;
import com.google.common.hash.BloomFilter;
import com.google.common.hash.Funnels;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.PageRequest;

import java.nio.charset.Charset;
import java.util.List;

/**
 * <p>标题: </p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2019</p>
 * <p>公司: 智业软件股份有限公司</p>
 *
 * @version: 1.0
 * @author: ruansheng
 * @date 5/15/2019
 */
@Configuration
@Slf4j
public class BloomFilterBean {

    private static final int PAGE_SIZE = 100000;
    private final MusicService musicService;
    private final IgnoreMusicItemService ignoreMusicItemService;

    public BloomFilterBean(MusicService musicService, IgnoreMusicItemService ignoreMusicItemService) {
        this.musicService = musicService;
        this.ignoreMusicItemService = ignoreMusicItemService;
    }

    @Bean(BloomFilterConstrain.ALL_MUSIC_ID)
    public BloomFilter<String> allMusicIdFilter() {
        log.info("初始化音乐ID布隆过滤器");
        BloomFilter<String> bloomFilter = BloomFilter
                .create(Funnels.stringFunnel(Charset.defaultCharset()), 1024 * 1024 * 2, 0.0001);
        long musicCount = musicService.count();
        int pageCount = (int) (musicCount / PAGE_SIZE) + 1;
        for (int i = 0; i < pageCount; i++) {
            List<String> idList = musicService.findAllId(PageRequest.of(i, PAGE_SIZE));
            idList.forEach(bloomFilter::put);
            log.info("正在初始化布隆过滤器 , 进度:{}/{} , 数量:{}", i + 1, pageCount, i * PAGE_SIZE + idList.size());
        }
        System.gc();
        log.info("初始化布隆过滤器成功,误判率:{}", bloomFilter.expectedFpp());
        return bloomFilter;
    }

    @Bean(BloomFilterConstrain.EXCLUDE_MUSIC_ID)
    public BloomFilter<String> excludeMusicIdFilter() {
        BloomFilter<String> bloomFilter = BloomFilter
                .create(Funnels.stringFunnel(Charset.defaultCharset()), 1024 * 10, 0.0001);
        List<String> ids = ignoreMusicItemService.findAllByType(IgnoreTypeEnum.MUSIC_ID);
        ids.forEach(bloomFilter::put);
        return bloomFilter;
    }
}
