package com.ruanx.netease.bean;

import java.util.Date;

/**
 * <p>标题: </p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2019</p>
 * <p>公司: 智业软件股份有限公司</p>
 *
 * @version: 1.0
 * @author: ruansheng
 * @date 5/14/2019
 */
public class GlobalProxy {

    public static volatile String HOST;

    public static volatile int PORT;

    public static volatile Date EXPIRED_TIME;

    public static boolean isExpired() {
        if (EXPIRED_TIME == null) {
            return false;
        }
        return EXPIRED_TIME.after(new Date());
    }

    public static void checkValid() {
        if (isExpired()) {
            HOST = "";
            EXPIRED_TIME = null;
        }
    }
}
