package com.ruanx.netease.bean;

import java.io.IOException;

/**
 * <p>标题: </p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2019</p>
 * <p>公司: 智业软件股份有限公司</p>
 *
 * @version: 1.0
 * @author: ruansheng
 * @date 5/14/2019
 */
public interface ProxyIpRefresh {
    void refreshGlobalProxy() throws IOException, InterruptedException;

    void reset();
}
