package com.ruanx.netease.exception;

/**
 * @Author: Ruan Sheng
 * @Date: 2018/4/23 15:12
 */
public class BaseMusicException extends Exception {
    public BaseMusicException() {
    }

    public BaseMusicException(String message) {
        super(message);
    }

    public BaseMusicException(String message, Throwable cause) {
        super(message, cause);
    }

    public BaseMusicException(Throwable cause) {
        super(cause);
    }

    public BaseMusicException(String message, Throwable cause, boolean enableSuppression,
                              boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
