package com.ruanx.netease.exception;

/**
 * @author Ruan Sheng
 * @date 2018/4/24 18:41
 */
public class MusicUrlParserException extends BaseMusicException {
    public static final String MUSIC_URL_PARSER_EXCEPTION = "音乐地址解析异常";

    public MusicUrlParserException() {
        super(MUSIC_URL_PARSER_EXCEPTION);
    }
}
