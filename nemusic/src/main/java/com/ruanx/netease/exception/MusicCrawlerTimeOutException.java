package com.ruanx.netease.exception;

/**
 * @author Ruan Sheng
 * @date 2018/4/23 12:47
 */
public class MusicCrawlerTimeOutException extends BaseMusicException {
    public static final String OBTAIN_PLAY_LIST_INFO_TIME_OUT = "获取歌单信息超时!";
    public static final String OBTAIN_RELEVANCE_PLAY_LIST_INFO_TIME_OUT = "获取关联歌单信息超时!";
    public static final String OBTAIN_SIMILAR_MUSIC_INFO_TIME_OUT = "获取相似音乐信息超时!";
    public static final String OBTAIN_MUSIC_INFO_TIME_OUT = "获取音乐信息超时!";

    public MusicCrawlerTimeOutException() {
    }

    public MusicCrawlerTimeOutException(String message) {
        super(message);
    }

    public MusicCrawlerTimeOutException(String message, Throwable cause) {
        super(message, cause);
    }

    public MusicCrawlerTimeOutException(Throwable cause) {
        super(cause);
    }

    public MusicCrawlerTimeOutException(String message, Throwable cause, boolean enableSuppression,
                                        boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
