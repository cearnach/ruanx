package com.ruanx.netease.exception;

/**
 * 全局异常基类,继承此类方便做全局异常处理
 *
 * @author 阮胜
 * @date 2018/11/1 21:49
 */
public class BaseException extends Exception {

    private static final long serialVersionUID = -5870037929167675840L;

    public BaseException() {
    }

    public BaseException(String message) {
        super(message);
    }

    public BaseException(String message, Throwable cause) {
        super(message, cause);
    }

    public BaseException(Throwable cause) {
        super(cause);
    }

    public BaseException(String message, Throwable cause, boolean enableSuppression,
                         boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
