package com.ruanx.netease.exception;

/**
 * @Author: Ruan Sheng
 * @Date: 2018/4/23 15:03
 */
public class MusicNotFoundException extends BaseMusicException {
    public static final String MUSIC_NOT_FOUND = "找不到指定音乐!";

    public MusicNotFoundException() {
        super(MUSIC_NOT_FOUND);
    }


}
