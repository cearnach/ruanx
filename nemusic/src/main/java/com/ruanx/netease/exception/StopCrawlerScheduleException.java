package com.ruanx.netease.exception;

/**
 * <p>标题: </p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2019</p>
 * <p>公司: 智业软件股份有限公司</p>
 *
 * @version: 1.0
 * @author: ruansheng
 * @date 5/13/2019
 */
public class StopCrawlerScheduleException extends BaseException {
    public StopCrawlerScheduleException() {
        super("爬虫数据频繁,停止爬虫任务");
    }
}
