package com.ruanx.netease.exception;

/**
 * @Author: Ruan Sheng
 * @Date: 2018/4/23 15:06
 */
public class MusicCopyrightException extends BaseMusicException {
    public static final String COPYRIGHT = "版权问题，该音乐已在网易云音乐下架.";

    public MusicCopyrightException() {
        super(COPYRIGHT);
    }

}
