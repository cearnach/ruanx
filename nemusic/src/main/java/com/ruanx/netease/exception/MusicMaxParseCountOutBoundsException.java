package com.ruanx.netease.exception;

import com.ruanx.netease.crawler.PlaylistCrawler;

/**
 * @Author: Ruan Sheng
 * @Date: 2018/4/23 15:05
 */
public class MusicMaxParseCountOutBoundsException extends BaseMusicException {
    public static final String MUSIC_MAX_PARSE_COUNT = "歌单里面的音乐数超过" + PlaylistCrawler.MAX_UPDATE_SIZE + "," +
            "拒绝解析!";

    public MusicMaxParseCountOutBoundsException() {
        super(MUSIC_MAX_PARSE_COUNT);
    }


}
