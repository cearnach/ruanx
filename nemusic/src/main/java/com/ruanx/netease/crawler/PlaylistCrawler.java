package com.ruanx.netease.crawler;

import com.ruanx.netease.entity.Music;
import com.ruanx.netease.entity.Playlist;
import com.ruanx.netease.entity.page.PlaylistPageInfo;
import com.ruanx.netease.exception.MusicCrawlerTimeOutException;
import com.ruanx.netease.exception.MusicMaxParseCountOutBoundsException;
import com.ruanx.netease.multithreading.DefaultMultithreadingCrawler;
import com.ruanx.netease.property.CrawlerConstants;
import com.ruanx.netease.util.GlobalThreadPool;
import com.ruanx.netease.util.JsoupUtil;
import com.ruanx.netease.util.MonitorTaskUtil;
import com.ruanx.netease.util.NumberConverterUtil;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.nodes.Document;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * 歌单爬虫类
 *
 * @author Ruan Sheng
 * @date 2018/4/19 13:13
 */
@RestController
@RequestMapping("/crawler/playlist")
@Slf4j
public class PlaylistCrawler {

    public static final String PLAYLIST_URL = "https://music.163.com/discover/playlist/?order=new&cat=%s" +
            "&limit=35&offset=%d";
    /**
     * 歌单里面的音乐数量不能超过该值
     */
    public static final int MAX_UPDATE_SIZE = 1000;
    private static final Pattern PLAYLIST_ID_PATTER = Pattern.compile("/playlist\\?id=(\\d+)");

    /**
     * 解析歌单信息
     *
     * @param doc 网页文档对象
     * @return
     */
    private static Playlist parsePlaylistInfo(Document doc) {
        String title = doc.select("div .tit h2").text();
        int playCount = Integer.parseInt(doc.select("#play-count").text());
        String favoriteCountWithUnit = doc.select(".u-btni.u-btni-fav i").text().replace("(", "").replace(
                ")", "");
        int favoriteCount = NumberConverterUtil.convert(favoriteCountWithUnit);
        int commentCount = Integer.parseInt(doc.select("#cnt_comment_count").text());
        int musicCount = Integer.parseInt(doc.select("#playlist-track-count").text());
        return new Playlist(title, null, musicCount, playCount, favoriteCount, commentCount);
    }

    /**
     * 解析关联歌单id
     *
     * @param doc 网页文档对象
     * @return 关联歌单id集合
     */
    private static List<String> parseRelevancePlaylistId(Document doc) {
        List<String> playListIdList = doc.select("div .info .sname").eachAttr("href");
        playListIdList.replaceAll(id -> id.replace("/playlist?id=", ""));
        return playListIdList;
    }

    /**
     * 根据歌单id获取歌单页面信息
     *
     * @param playListId
     * @return
     * @throws IOException
     */
    public static PlaylistPageInfo obtainPlaylistPageInfoById(String playListId) throws IOException {
        return obtainPlaylistPageInfoById(playListId, true, true, true);
    }

    /**
     * 根据歌单id获取歌单信息以及歌单里面的音乐集合
     *
     * @param playListId          歌单id
     * @param isParseMusicInfo    是否解析歌单里的音乐详细信息
     * @param isParsePlaylistInfo 是否解析歌单信息
     * @return 歌单信息和歌单里面的音乐集合
     */
    private static PlaylistPageInfo obtainPlaylistPageInfoById(String playListId, boolean isParseMusicInfo
            , boolean isParsePlaylistInfo, boolean isParseRelevancePlaylistId) throws IOException {
        String url = CrawlerConstants.URL_MUSIC.concat(playListId);
        Document doc;
        doc = JsoupUtil.connect(url).get();
        PlaylistPageInfo playListPageInfo = new PlaylistPageInfo();
        if (isParseMusicInfo) {
            List<String> idList = doc.select("a[href*=/song?id=]").eachAttr("href");
            idList.replaceAll(id -> id.replace("/song?id=", ""));
            playListPageInfo.setMusicIdList(idList);
        }
        if (isParsePlaylistInfo) {
            Playlist playList = parsePlaylistInfo(doc);
            playListPageInfo.setPlaylist(playList);
            playList.setId(playListId);
        }
        if (isParseRelevancePlaylistId) {
            List<String> playListIdList = parseRelevancePlaylistId(doc);
            playListPageInfo.setRelevancePlaylistIdList(playListIdList);
        }
        if (playListPageInfo.getPlaylist() == null && CollectionUtils.isEmpty(playListPageInfo.getMusicIdList())) {
            log.error("获取歌单信息失败");
        }
        return playListPageInfo;
    }

    /**
     * 根据歌单id获取歌单里面的音乐ID集合
     *
     * @param playListId 歌单id
     * @return
     */
    public static List<String> obtainMusicIdListByPlaylistId(String playListId) {
        try {
            return obtainPlaylistPageInfoById(playListId, true, false, false).getMusicIdList();
        } catch (IOException e) {
            log.error("根据歌单id获取歌单里面的音乐ID集合失败,原因:{}", e.getMessage());
            return Collections.emptyList();
        }
    }

    /**
     * 根据歌单Id获取音乐详细信息集合
     *
     * @param playListId 歌单id
     * @return
     * @throws InterruptedException
     */
    public static Set<Music> obtainMusicInfoSetByPlaylistId(String playListId) throws IOException,
            InterruptedException
            , MusicMaxParseCountOutBoundsException {
        if (StringUtils.isEmpty(playListId)) {
            return Collections.emptySet();
        }
        List<String> musicIdList = PlaylistCrawler.obtainMusicIdListByPlaylistId(playListId);
        int size = musicIdList.size();
        if (size >= MAX_UPDATE_SIZE) {
            throw new MusicMaxParseCountOutBoundsException();
        }
        CountDownLatch latch = new CountDownLatch(size);
        ScheduledFuture<?> future = MonitorTaskUtil.monitorCrawler(latch, "根据歌单Id获取音乐集合", size);
        Set<Music> musicSet = Collections.synchronizedSet(new HashSet<>());
        musicIdList.forEach(id -> GlobalThreadPool.POOL
                .execute(new DefaultMultithreadingCrawler<>(id, musicSet, latch
                        , () -> MusicCrawler.obtainMusicInfoById(id))));
        //如果超过指定时间还未获取完毕
        if (!latch.await(CrawlerConstants.CRAWLER_TIMEOUT, TimeUnit.MINUTES)) {
            log.error("获取音乐超时");
        }
        MonitorTaskUtil.printMonitorTaskLog("根据歌单Id获取音乐集合 已完成:%d , 总任务数:%d , 完成百分比%.2f%%", size, size);
        future.cancel(true);
        return musicSet;
    }

    /**
     * 根据歌单id集合获取所有集合里面的音乐信息集合
     *
     * @param playlistIdList
     * @return
     */
    public static Set<Music> obtainMusicInfoSetByPlaylistIdList(List<String> playlistIdList) throws InterruptedException {
        if (StringUtils.isEmpty(playlistIdList)) {
            return Collections.emptySet();
        }
        int size = playlistIdList.size();
        CountDownLatch latch = new CountDownLatch(size);
        Set<Music> musicSet = Collections.synchronizedSet(new HashSet<>(size));
        playlistIdList.forEach(playlistId -> GlobalThreadPool.POOL
                .execute(new DefaultMultithreadingCrawler<>(playlistId, musicSet, latch
                        , () -> obtainMusicInfoSetByPlaylistId(playlistId))));
        if (!latch.await(CrawlerConstants.CRAWLER_TIMEOUT, TimeUnit.MINUTES)) {
            log.error(MusicCrawlerTimeOutException.OBTAIN_MUSIC_INFO_TIME_OUT);
        }
        return musicSet;
    }

    /**
     * 根据歌单id获取歌单信息
     *
     * @param playListId 歌单id
     * @return
     * @throws IOException
     */
    public static Playlist obtainPlaylistInfoByPlaylistId(String playListId) throws IOException {
        return obtainPlaylistPageInfoById(playListId, false, true, false).getPlaylist();
    }

    public static List<String> obtainRelevancePlaylistIdListByPlaylistId(String playListId) throws IOException {
        return obtainPlaylistPageInfoById(playListId, false, false, true).getRelevancePlaylistIdList();
    }

    /**
     * 根据歌单id集合批量获取歌单信息
     *
     * @param playListIdList 歌单id集合
     * @return
     * @throws InterruptedException
     */
    public static List<Playlist> obtainPlaylistInfoListByPlaylistIdList(List<String> playListIdList)
            throws InterruptedException {
        if (CollectionUtils.isEmpty(playListIdList)) {
            return Collections.emptyList();
        }
        int size = playListIdList.size();
        CountDownLatch latch = new CountDownLatch(size);
        List<Playlist> playListInfoList = Collections.synchronizedList(new ArrayList<>(size));
        playListIdList.forEach(playListId -> GlobalThreadPool.POOL
                .execute(new DefaultMultithreadingCrawler<>(playListId, playListInfoList, latch
                        , () -> obtainPlaylistInfoByPlaylistId(playListId))));
        if (!latch.await(CrawlerConstants.CRAWLER_TIMEOUT, TimeUnit.MINUTES)) {
            log.error(MusicCrawlerTimeOutException.OBTAIN_MUSIC_INFO_TIME_OUT);
        }
        return playListInfoList;
    }


    /**
     * 根据歌单id获取关联歌单id集合
     *
     * @param playlistId
     * @return
     * @throws IOException
     */
    public static List<Playlist> obtainRelevancePlaylistInfoListByPlayListId(String playlistId)
            throws IOException, InterruptedException {
        List<String> relevancePlaylistIdList = obtainRelevancePlaylistIdListByPlaylistId(playlistId);
        return obtainPlaylistInfoListByPlaylistIdList(relevancePlaylistIdList);
    }

    /**
     * 根据歌单id集合批量获取关联歌单信息集合
     *
     * @param playlistIdList
     * @return
     */
    public static List<Playlist> obtainRelevancePlaylistInfoListByPlaylistIdList(List<String> playlistIdList)
            throws InterruptedException {
        List<String> idList = obtainRelevancePlaylistIdListByPlaylistIdList(playlistIdList);
        return obtainPlaylistInfoListByPlaylistIdList(idList);
    }


    /**
     * 根据歌单id集合批量获取关联歌单id集合
     *
     * @param playlistIdList
     * @return
     */
    public static List<String> obtainRelevancePlaylistIdListByPlaylistIdList(List<String> playlistIdList)
            throws InterruptedException {
        int size = playlistIdList.size();
        CountDownLatch latch = new CountDownLatch(size);
        List<String> relevancePlaylistIdList = Collections.synchronizedList(new ArrayList<>(size * 3));
        playlistIdList.forEach(id -> GlobalThreadPool.POOL
                .execute(new DefaultMultithreadingCrawler<>(id, relevancePlaylistIdList, latch
                        , () -> obtainRelevancePlaylistIdListByPlaylistId(id))));
        if (!latch.await(CrawlerConstants.CRAWLER_TIMEOUT, TimeUnit.MINUTES)) {
            log.error(MusicCrawlerTimeOutException.OBTAIN_PLAY_LIST_INFO_TIME_OUT);
        }
        return relevancePlaylistIdList.stream().distinct().collect(Collectors.toList());
    }

    /**
     * 根据歌单id集合获取音乐id集合
     *
     * @param playlistIdList
     * @return
     * @throws InterruptedException
     */
    public static List<String> obtainMusicIdListByPlaylistIdList(List<String> playlistIdList)
            throws InterruptedException {
        if (CollectionUtils.isEmpty(playlistIdList)) {
            return Collections.emptyList();
        }
        int size = playlistIdList.size();
        CountDownLatch latch = new CountDownLatch(size);
        List<String> musicIdList = Collections.synchronizedList(new ArrayList<>(size));
        playlistIdList.forEach(id -> GlobalThreadPool.POOL
                .execute(new DefaultMultithreadingCrawler<>(id, musicIdList, latch
                        , () -> obtainMusicIdListByPlaylistId(id))));
        if (!latch.await(CrawlerConstants.CRAWLER_TIMEOUT, TimeUnit.MINUTES)) {
            log.error(MusicCrawlerTimeOutException.OBTAIN_MUSIC_INFO_TIME_OUT);
        }
        return musicIdList.stream().distinct().collect(Collectors.toList());
    }

    /**
     * 根据歌单名称获取对应页面的html文本
     *
     * @param playlistType 歌单类型,比如:全部,欧美,怀旧
     * @return html文本
     */
    public static String obtainPlaylistHtmlByName(String playlistType, int offset) throws IOException {
        String url = String.format(PLAYLIST_URL, playlistType, offset);
        return JsoupUtil.connect(url).get().html();

    }

    /**
     * 根据歌单页面HTML获取歌单ID
     *
     * @param html
     * @return
     */
    public static Set<String> obtainPlaylistIdByHtml(String html) {
        if (StringUtils.isEmpty(html)) {
            return Collections.emptySet();
        }
        Matcher playListIdMather = PLAYLIST_ID_PATTER.matcher(html);
        Set<String> playlistIdSet = new HashSet<>(playListIdMather.groupCount());
        while (playListIdMather.find()) {
            String id = playListIdMather.group(1);
            playlistIdSet.add(id);
        }
        return playlistIdSet;
    }
}
