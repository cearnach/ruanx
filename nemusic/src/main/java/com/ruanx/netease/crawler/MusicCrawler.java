package com.ruanx.netease.crawler;

import com.ruanx.netease.entity.Music;
import com.ruanx.netease.entity.Playlist;
import com.ruanx.netease.entity.page.MusicPageInfo;
import com.ruanx.netease.exception.MusicCopyrightException;
import com.ruanx.netease.exception.MusicCrawlerTimeOutException;
import com.ruanx.netease.exception.MusicUrlParserException;
import com.ruanx.netease.multithreading.DefaultMultithreadingCrawler;
import com.ruanx.netease.property.CrawlerConstants;
import com.ruanx.netease.util.EncryptUtil;
import com.ruanx.netease.util.GlobalThreadPool;
import com.ruanx.netease.util.HttpUtil;
import com.ruanx.netease.util.JsoupUtil;
import com.ruanx.netease.util.MonitorTaskUtil;
import com.ruanx.netease.util.MusicUtil;
import com.ruanx.netease.util.StringUtils;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.nodes.Document;
import org.springframework.http.HttpStatus;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;


/**
 * 音乐爬虫类
 *
 * @author Ruan Sheng
 * @date 2018/4/19 13:13
 */
@Slf4j
public class MusicCrawler {


    /**
     * 根据音乐Id获取音乐信息,只能获取title,singer,musicId,comment
     *
     * @param musicId
     * @return
     */
    public static Music obtainMusicInfoById(String musicId) throws Exception {
        return obtainMusicPageInfoById(musicId, true, true, false, false, false).getMusic();
    }

    /**
     * 多线程获取音乐信息
     *
     * @param musicIdList
     * @return
     */
    public static Set<Music> obtainMusicInfoByMusicIdList(Collection<String> musicIdList) throws InterruptedException, MusicCrawlerTimeOutException {
        if (CollectionUtils.isEmpty(musicIdList)) {
            return Collections.emptySet();
        }
        int size = musicIdList.size();
        Set<Music> musicSet = Collections.synchronizedSet(new HashSet<>(size));
        CountDownLatch latch = new CountDownLatch(size);
        musicIdList.forEach(musicId -> GlobalThreadPool.POOL
                .execute(new DefaultMultithreadingCrawler<>(musicId, musicSet, latch
                        , () -> obtainMusicInfoById(musicId))));
        String printTaskStr = "根据音乐id集合获取音乐信息";
        ScheduledFuture<?> future = MonitorTaskUtil.monitorCrawler(latch, printTaskStr, size);
        if (!latch.await(CrawlerConstants.CRAWLER_TIMEOUT, TimeUnit.MINUTES)) {
            future.cancel(true);
            throw new MusicCrawlerTimeOutException(MusicCrawlerTimeOutException.OBTAIN_MUSIC_INFO_TIME_OUT);
        }
        future.cancel(true);
        log.info(printTaskStr.concat(" 完成!"));
        return musicSet;
    }

    /**
     * 获取指定音乐页面信息
     *
     * @param musicId                        音乐id
     * @param isParseMusicInfo               是否解析音乐信息
     * @param isParseMusicCommentCount       是否解析评论数
     * @param isParseRelevancePlaylistIdList 是否解析关联歌单id
     * @param isParseSimilarMusicIdList      是否解析相似音乐
     * @param isParseMusicDuration           是否解析音乐时间长度
     * @return 返回指定音乐页面的信息
     * @throws IOException
     */
    public static MusicPageInfo obtainMusicPageInfoById(String musicId, boolean isParseMusicInfo
            , boolean isParseMusicCommentCount, boolean isParseRelevancePlaylistIdList,
                                                        boolean isParseSimilarMusicIdList,
                                                        boolean isParseMusicDuration) throws Exception {
        String url = CrawlerConstants.URL_MUSIC_INFO.concat(musicId);
        Document doc = JsoupUtil.connect(url).get();
        MusicPageInfo musicPageInfo = new MusicPageInfo();
        //解析音乐信息
        if (isParseMusicInfo) {
            String singer = doc.select("span").attr("title");
            String title = doc.select("a").attr("data-res-name");
            String duration = "-1";
            //解析播放时间
            if (isParseMusicDuration) {
                JSONObject jsonObject = obtainMusicUrlJsonData(musicId);
                if (HttpStatus.NOT_FOUND.value() == (int) jsonObject.get("code")) {
                    throw new MusicUrlParserException();
                }
                duration = MusicUtil.formatMusicLength((int) jsonObject.get("size"), (int) jsonObject.get(
                        "br"));
            }
            //解析音乐评论数
            int comment = isParseMusicCommentCount ? obtainComment(musicId) : 0;
            Music music = new Music(musicId, title, comment, singer, duration);
            musicPageInfo.setMusic(music);

        }
        //解析与音乐关联的歌单id集合
        if (isParseRelevancePlaylistIdList) {
            List<String> playListIdList = doc.select("div .info .f-thide a").eachAttr("data-res-id");
            musicPageInfo.setRelevancePlaylistIdList(playListIdList);
        }
        //解析与音乐相似的音乐id集合
        if (isParseSimilarMusicIdList) {
            List<String> similarMusicIdList = doc.select("div .txt .f-thide a").eachAttr("data-res-id");
            musicPageInfo.setSimilarMusicIdList(similarMusicIdList);
        }


        return musicPageInfo;
    }

    /**
     * 获取所有音乐页面信息
     *
     * @param musicId 音乐id
     * @return 返回指定音乐页面的所有信息
     * @throws IOException
     */
    public static MusicPageInfo obtainMusicPageInfoById(String musicId) throws Exception {
        return obtainMusicPageInfoById(musicId, true, true, true, true, true);
    }


    /**
     * 根据音乐Id获取评论数
     *
     * @param musicId
     * @return
     */
    public static int obtainComment(String musicId) {
        String url = CrawlerConstants.URL_COMMENT_COUNT.concat(musicId);
        try {
            String result = HttpUtil.post(url, CrawlerConstants.PARAM_MAP);
            if (StringUtils.isEmpty(result)) {
                return -1;
            }
            JSONObject jsonObj = JSONObject.parseObject(result);
            Integer total = jsonObj.getInteger("total");
            return total == null ? -1 : total;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static void main(String[] args) {
        int i = obtainComment("25906124");
        System.out.println(i);
    }

    /**
     * 根据音乐id获取关联歌单id集合
     *
     * @param musicId
     * @return
     * @throws IOException
     */
    public static List<String> obtainRelevancePlaylistIdList(String musicId) throws Exception {
        return obtainMusicPageInfoById(musicId, false, false, true, false, false).getRelevancePlaylistIdList();
    }


    /**
     * 根据音乐id获取与音乐关联的歌单信息集合
     *
     * @param musicId
     * @return
     */
    public static List<Playlist> obtainRelevancePlaylistByMusicId(String musicId) throws Exception {
        List<String> relevancePlaylistIdList = obtainRelevancePlaylistIdList(musicId);
        return PlaylistCrawler.obtainPlaylistInfoListByPlaylistIdList(relevancePlaylistIdList);
    }


    /**
     * 根据音乐id获取相似音乐id集合
     *
     * @param musicId 音乐id
     * @return
     * @throws IOException
     */
    public static List<String> obtainSimilarMusicIdListByMusicId(String musicId) throws Exception {
        return obtainMusicPageInfoById(musicId, false, false, false, true, false).getSimilarMusicIdList();
    }

    /**
     * 根据音乐id集合批量获取相似音乐id集合
     *
     * @param musicIdList
     * @return
     * @throws InterruptedException
     */
    public static List<String> obtainSimilarMusicIdListByMusicIdList(List<String> musicIdList) throws InterruptedException, MusicCrawlerTimeOutException {
        List<String> similarMusicIdList = Collections.synchronizedList(new ArrayList<>(musicIdList.size()));
        CountDownLatch latch = new CountDownLatch(musicIdList.size());
        musicIdList.forEach(musicId -> GlobalThreadPool.POOL
                .execute(new DefaultMultithreadingCrawler<>(musicId, similarMusicIdList, latch
                        , () -> obtainSimilarMusicIdListByMusicId(musicId))));
        if (!latch.await(CrawlerConstants.CRAWLER_TIMEOUT, TimeUnit.MINUTES)) {
            throw new MusicCrawlerTimeOutException(MusicCrawlerTimeOutException.OBTAIN_SIMILAR_MUSIC_INFO_TIME_OUT);
        }
        //对集合去重复
        return similarMusicIdList.stream()
                .distinct()
                .collect(Collectors.toList());
    }

    /**
     * 根据音乐id获取相似音乐集合
     *
     * @param musicId 音乐id
     * @return 相似音乐id集合
     * @throws IOException
     */
    public static Set<Music> obtainSimilarMusicByMusicId(String musicId) throws Exception {
        List<String> similarMusicIdList = obtainMusicPageInfoById(musicId).getSimilarMusicIdList();
        return obtainMusicInfoByMusicIdList(similarMusicIdList);
    }

    /**
     * 根据 "音乐id集合" 批量获取相似音乐集合
     *
     * @param musicIdList 音乐id集合
     * @return 相似音乐集合
     */
    public static Set<Music> obtainSimilarMusicByMusicIdList(List<String> musicIdList) throws InterruptedException, MusicCrawlerTimeOutException {
        int itemCount = musicIdList.size();
        Set<Music> similarMusicSet = Collections.synchronizedSet(new HashSet<>(itemCount));
        CountDownLatch latch = new CountDownLatch(itemCount);
        musicIdList.forEach(id -> GlobalThreadPool.POOL
                .execute(new DefaultMultithreadingCrawler<>(id, similarMusicSet, latch, () -> {
                    List<String> similarMusicIdList = obtainSimilarMusicIdListByMusicId(id);
                    return obtainMusicInfoByMusicIdList(similarMusicIdList);
                })));
        if (!latch.await(CrawlerConstants.CRAWLER_TIMEOUT, TimeUnit.MINUTES)) {
            throw new MusicCrawlerTimeOutException(MusicCrawlerTimeOutException.OBTAIN_SIMILAR_MUSIC_INFO_TIME_OUT);
        }
        return similarMusicSet;
    }


    /**
     * 根据MusicId取音乐下载地址,文件大小等json信息
     * 关键参数:{url,code,size,br}
     *
     * @param musicId 音乐id
     * @return 解析后的音乐下载地址
     */
    public static JSONObject obtainMusicUrlJsonData(String musicId) throws Exception {
        String encStr = "{\"ids\":\"[" + musicId + "]\",\"br\":128000,\"csrf_token\":\"\"}";
        //使用js方式加密字符串
        Map<String, String> paramMap = EncryptUtil.encryptByJs(encStr);
        String url = CrawlerConstants.URL_MUSIC_DOWNLOAD;
        Document doc = JsoupUtil.connect(url).data(paramMap).cookie("_ntes_nuid",
                "3ae6713385f9ea37bda8c95dca5ea6a6").post();
        String jsonStr = doc.body().html();
        JSONObject urlJsonObj = JSONObject.parseObject(jsonStr);
        int code = Integer.parseInt(urlJsonObj.get("code").toString());
        if (code == HttpStatus.NOT_FOUND.value()) {
            throw new MusicUrlParserException();
        } else if (code == -110) {
            throw new MusicCopyrightException();
        }
        return JSONObject.parseArray(urlJsonObj.get("data").toString()).getJSONObject(0);
    }

    /**
     * 根据MusicId取音乐下载地址
     *
     * @param musicId 音乐id
     * @return 解析后的音乐下载地址
     */
    public static String obtainMusicUrl(String musicId) throws Exception {
        JSONObject jsonObject = obtainMusicUrlJsonData(musicId);
        return jsonObject.get("url").toString();
    }

    /**
     * 根据音乐id集合批量获取音乐播放时间
     *
     * @param musicIdList 音乐id集合
     * @return 音乐id与播放时间的映射表
     * @throws InterruptedException
     */
    public static Map<String, String> obtainMusicDurationListByMusicIdList(List<String> musicIdList) throws InterruptedException {
        int size = musicIdList.size();
        Map<String, String> durationMap = Collections.synchronizedMap(new HashMap<>(size));
        CountDownLatch latch = new CountDownLatch(size);
        musicIdList.forEach(musicId -> GlobalThreadPool.POOL.execute(() -> {
            String duration;
            try {
                JSONObject data = obtainMusicUrlJsonData(musicId);
                duration = MusicUtil.formatMusicLength((int) data.get("size"), (int) data.get("br"));
            } catch (Exception e) {
                duration = "-1";
                log.error("{} , MusicId : {}", e.getMessage(), musicId);
            } finally {
                latch.countDown();
            }
            durationMap.put(musicId, duration);
        }));
        ScheduledFuture<?> future = MonitorTaskUtil.monitorCrawler(latch, "更新音乐时间", size);
        try {
            if (!latch.await(CrawlerConstants.CRAWLER_TIMEOUT, TimeUnit.MINUTES)) {
                log.info(MusicCrawlerTimeOutException.OBTAIN_MUSIC_INFO_TIME_OUT);
            }
        } finally {
            future.cancel(true);
        }
        return durationMap;
    }


}
