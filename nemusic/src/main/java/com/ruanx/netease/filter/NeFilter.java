package com.ruanx.netease.filter;

import com.ruanx.netease.entity.Music;

import java.util.Collection;
import java.util.List;

/**
 * <p>标题: </p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2019</p>
 * <p>公司: 智业软件股份有限公司</p>
 *
 * @version: 1.0
 * @author: ruansheng
 * @date 5/13/2019
 */
public interface NeFilter {

    boolean existsByMusicId(String musicId);

    boolean existsByPlaylistId(String playlistId);

    boolean existsBySingerName(String singerName);

    List<String> existsByMusicIdBatch(Collection<String> musicIds);

    List<String> notExistsByMusicIdBatch(Collection<String> musicIds);

    List<String> existsByPlaylistIdBatch(Collection<String> playlistIds);

    List<String> notExistsByPlaylistIdBatch(Collection<String> playlistIds);

    void onMusicSaved(Music music);

}
