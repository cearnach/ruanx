package com.ruanx.netease.filter.impl;

import com.ruanx.netease.entity.IgnoreMusicItemId;
import com.ruanx.netease.entity.Music;
import com.ruanx.netease.enumeration.IgnoreTypeEnum;
import com.ruanx.netease.filter.NeFilter;
import com.ruanx.netease.service.IgnoreMusicItemService;
import com.google.common.hash.BloomFilter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>标题: </p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2019</p>
 * <p>公司: 智业软件股份有限公司</p>
 *
 * @version: 1.0
 * @author: ruansheng
 * @date 5/15/2019
 */
@Component
public class NeBloomFilter implements NeFilter {

    private final IgnoreMusicItemService ignoreMusicItemService;
    private BloomFilter<String> bloomFilter;

    public NeBloomFilter(@Qualifier("ALL_MUSIC_ID_BLOOM_FILTER") BloomFilter<String> bloomFilter,
                         IgnoreMusicItemService ignoreMusicItemService) {
        this.bloomFilter = bloomFilter;
        this.ignoreMusicItemService = ignoreMusicItemService;
    }


    @Override
    public boolean existsByMusicId(String musicId) {
        return bloomFilter.mightContain(musicId);
    }

    @Override
    public boolean existsByPlaylistId(String playlistId) {
        return ignoreMusicItemService.existsById(new IgnoreMusicItemId(playlistId, IgnoreTypeEnum.PLAYLIST_ID));
    }

    @Override
    public boolean existsBySingerName(String singerName) {
        return ignoreMusicItemService.existsById(new IgnoreMusicItemId(singerName, IgnoreTypeEnum.SINGER_NAME));
    }

    @Override
    public List<String> existsByMusicIdBatch(Collection<String> musicIds) {
        if (CollectionUtils.isEmpty(musicIds)) {
            return Collections.emptyList();
        }
        return musicIds.stream()
                .filter(musicId -> bloomFilter.mightContain(musicId))
                .collect(Collectors.toList());
    }

    @Override
    public List<String> notExistsByMusicIdBatch(Collection<String> musicIds) {
        if (CollectionUtils.isEmpty(musicIds)) {
            return Collections.emptyList();
        }
        return musicIds.stream()
                .filter(musicId -> !bloomFilter.mightContain(musicId))
                .collect(Collectors.toList());
    }

    @Override
    public List<String> existsByPlaylistIdBatch(Collection<String> playlistIds) {
        if (CollectionUtils.isEmpty(playlistIds)) {
            return Collections.emptyList();
        }
        return playlistIds.stream()
                .filter(playlistId -> ignoreMusicItemService
                        .existsById(new IgnoreMusicItemId(playlistId, IgnoreTypeEnum.PLAYLIST_ID)))
                .collect(Collectors.toList());
    }

    @Override
    public List<String> notExistsByPlaylistIdBatch(Collection<String> playlistIds) {
        if (CollectionUtils.isEmpty(playlistIds)) {
            return Collections.emptyList();
        }
        return playlistIds.stream()
                .filter(playlistId -> !ignoreMusicItemService
                        .existsById(new IgnoreMusicItemId(playlistId, IgnoreTypeEnum.PLAYLIST_ID)))
                .collect(Collectors.toList());
    }

    @Override
    public void onMusicSaved(Music music) {
        bloomFilter.put(music.getId());
    }
}
