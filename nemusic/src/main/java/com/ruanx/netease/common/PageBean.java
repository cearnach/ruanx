package com.ruanx.netease.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

/**
 * @author 阮胜
 * @date 2018/9/142 23:35
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PageBean {
    @Min(0)
    private int page = 0;
    @Min(0)
    @Max(10)
    private int size = 10;

    public void setPage(int page) {
        this.page = page < 1 ? 0 : page - 1;
    }

    public Pageable toPageable() {
        return PageRequest.of(this.page, this.size);
    }

    public Pageable toPageable(Sort sort) {
        return PageRequest.of(this.page, this.size, sort);
    }

    public Pageable toPageable(Sort.Direction direction, String... properties) {
        return PageRequest.of(this.page, this.size);
    }

}
