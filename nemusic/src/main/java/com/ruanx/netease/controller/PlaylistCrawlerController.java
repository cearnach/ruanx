package com.ruanx.netease.controller;

import com.google.common.hash.BloomFilter;
import com.ruanx.netease.entity.Music;
import com.ruanx.netease.entity.Playlist;
import com.ruanx.netease.enumeration.IgnoreTypeEnum;
import com.ruanx.netease.exception.MusicCrawlerTimeOutException;
import com.ruanx.netease.exception.MusicMaxParseCountOutBoundsException;
import com.ruanx.netease.service.IgnoreMusicItemService;
import com.ruanx.netease.service.MusicCrawlerService;
import com.ruanx.netease.service.PlaylistCrawlerService;
import com.ruanx.netease.vo.ResultVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>标题: </p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2019</p>
 * <p>公司: 智业软件股份有限公司</p>
 *
 * @version: 1.0
 * @author: ruansheng
 * @date 5/16/2019
 */
@RestController
@RequestMapping("/crawler/playlist")
@Slf4j
public class PlaylistCrawlerController {

    private final PlaylistCrawlerService playlistCrawlerService;
    private final IgnoreMusicItemService ignoreMusicItemService;
    private final BloomFilter<String> bloomFilter;
    private final MusicCrawlerService musicCrawlerService;
    private Comparator<Music> musicCommentComparator = (m1, m2) -> m2.getComment() - m1.getComment();

    public PlaylistCrawlerController(PlaylistCrawlerService playlistCrawlerService,
                                     IgnoreMusicItemService ignoreMusicItemService, @Qualifier(
            "EXCLUDE_MUSIC_ID_BLOOM_FILTER") BloomFilter<String> bloomFilter,
                                     MusicCrawlerService musicCrawlerService) {
        this.playlistCrawlerService = playlistCrawlerService;
        this.ignoreMusicItemService = ignoreMusicItemService;
        this.bloomFilter = bloomFilter;
        this.musicCrawlerService = musicCrawlerService;
    }

    @GetMapping("/{playlistId}")
    public ResultVO<Object> obtainByPlaylistId(@PathVariable String playlistId, @RequestParam(value =
            "exclude", required = false) boolean exclude)
            throws InterruptedException, MusicCrawlerTimeOutException, MusicMaxParseCountOutBoundsException
            , IOException {
        log.info("根据歌单ID获取音乐列表");
        Set<Music> musicSet = playlistCrawlerService.obtainMusicInfoSetByPlaylistId(playlistId);
        if (exclude) {
            musicSet = filterMusic(musicSet);
        }
        List<Music> list = musicSet.stream().sorted(musicCommentComparator).collect(Collectors.toList());
        return ResultVO.builder().data(list).build();
    }

    private Set<Music> filterMusic(Set<Music> musicSet) {
        if (CollectionUtils.isEmpty(musicSet)) {
            return Collections.emptySet();
        }
        List<String> singers = ignoreMusicItemService.findAllByType(IgnoreTypeEnum.SINGER_NAME);
        return musicSet.stream()
                .filter(music -> !bloomFilter.mightContain(music.getId()))
                .filter(music -> singers != null && !singers.contains(music.getSinger()))
                .collect(Collectors.toSet());
    }

    @GetMapping("/similar/{playlistId}")
    public ResultVO<Object> obtainSimilarByPlaylistId(@PathVariable String playlistId,
                                                      @RequestParam(value = "exclude", required = false) boolean exclude)
            throws InterruptedException, MusicCrawlerTimeOutException, MusicMaxParseCountOutBoundsException
            , IOException {
        log.info("根据歌单ID获取歌单中的相似歌曲");
        List<String> musicIdList = playlistCrawlerService.obtainMusicInfoSetByPlaylistId(playlistId)
                .stream()
                .map(Music::getId)
                .collect(Collectors.toList());
        Set<Music> musicSet = musicCrawlerService.obtainSimilarMusicByMusicIdList(musicIdList)
                .stream()
                .filter(music -> !musicIdList.contains(music.getId()))
                .collect(Collectors.toSet());
        if (exclude) {
            musicSet = this.filterMusic(musicSet);
        }
        List<Music> list = musicSet.stream().sorted(musicCommentComparator).collect(Collectors.toList());
        return ResultVO.builder().data(list).build();
    }

    @GetMapping("/relevance/{playlistId}")
    public ResultVO<Object> obtainRelevanceByPlaylistId(@PathVariable String playlistId,
                                                        @RequestParam(value = "exclude", required = false) boolean exclude)
            throws IOException, InterruptedException, MusicCrawlerTimeOutException {
        log.info("根据歌单ID获取歌单中的获取关联歌曲");
        List<String> ignoreIdList = playlistCrawlerService.obtainMusicIdListByPlaylistId(playlistId);
        List<Playlist> playlists =
                playlistCrawlerService.obtainRelevancePlaylistInfoListByPlaylistId(playlistId);
        List<String> playlistIds = playlists.stream()
                .map(Playlist::getId)
                .collect(Collectors.toList());
        Set<Music> musicSet = playlistCrawlerService.obtainMusicInfoSetByPlaylistIdList(playlistIds)
                .stream()
                .filter(music -> !ignoreIdList.contains(music.getId()))
                .collect(Collectors.toSet());
        if (exclude) {
            musicSet = this.filterMusic(musicSet);
        }
        List<Music> list = musicSet.stream().sorted(musicCommentComparator).collect(Collectors.toList());
        return ResultVO.builder().data(list).build();
    }
}
