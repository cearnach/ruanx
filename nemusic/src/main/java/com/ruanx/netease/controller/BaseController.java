package com.ruanx.netease.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @Author: Ruan Sheng
 * @Date: 2018/4/26 11:04
 */
@Slf4j
@Controller
public class BaseController {
    @GetMapping("/music/*")
    public void everyPageOnMusic() {
    }

    @GetMapping("/api/*")
    public void everyPage() {

    }

    @GetMapping("/")
    public String home() {
        return "redirect:/index.html";
    }
}
