package com.ruanx.netease.controller;

import com.google.common.hash.BloomFilter;
import com.ruanx.netease.common.PageBean;
import com.ruanx.netease.entity.IgnoreMusicItem;
import com.ruanx.netease.entity.IgnoreMusicItemId;
import com.ruanx.netease.entity.Music;
import com.ruanx.netease.enumeration.IgnoreTypeEnum;
import com.ruanx.netease.exception.StopCrawlerScheduleException;
import com.ruanx.netease.schedule.CrawlerSchedule;
import com.ruanx.netease.service.IgnoreMusicItemService;
import com.ruanx.netease.service.MusicService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Example;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

/**
 * @author Ruan Sheng
 * @date 2018/4/20 19:07
 */
@RestController
@Validated
@RequestMapping("/music")
@Slf4j
public class MusicController {
    private final MusicService musicService;
    private final CrawlerSchedule crawlerSchedule;
    private final IgnoreMusicItemService ignoreMusicItemService;
    private final BloomFilter<String> bloomFilter;

    public MusicController(MusicService musicService, CrawlerSchedule crawlerSchedule,
                           IgnoreMusicItemService ignoreMusicItemService,
                           @Qualifier("EXCLUDE_MUSIC_ID_BLOOM_FILTER") BloomFilter<String> bloomFilter) {
        this.musicService = musicService;
        this.crawlerSchedule = crawlerSchedule;
        this.ignoreMusicItemService = ignoreMusicItemService;
        this.bloomFilter = bloomFilter;
    }


    @GetMapping("/list")
    public List<Music> list(@Valid PageBean pageBean) {
        return null;
    }

    /**
     * 排除id,添加到redis.用于过滤器
     *
     * @param ids
     * @return
     */
    @PostMapping("/exclude")
    public Integer exclude(@RequestBody String[] ids) {
        int count = 0;
        for (String id : ids) {
            if (!StringUtils.isEmpty(ids)) {
                ignoreMusicItemService.save(new IgnoreMusicItem(new IgnoreMusicItemId(id, IgnoreTypeEnum.MUSIC_ID)));
                count++;
                bloomFilter.put(id);
                log.info("Add exclude id to bloom filter , id:{}", id);
            }
        }
        return count;
    }

    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable String id) throws Exception {
        musicService.delete(id);
    }


    @GetMapping("/execute/crawler/playlistType")
    public String executeCrawler() throws IOException, StopCrawlerScheduleException {
        crawlerSchedule.executePlaylistCrawlerSchedule();
        return "开始根据歌单执行爬虫";
    }

    @GetMapping("/execute/crawler/updateComment")
    public String executeUpdateComment() throws IOException, InterruptedException {
        crawlerSchedule.updateCommentSchedule();
        return "开始更新评论";
    }

    @PostMapping("/save")
    public Music save(@RequestBody Music music) {
        return musicService.save(music);
    }

    @GetMapping("/list/example")
    public List<Music> listExample(@RequestBody Music music) {
        return musicService.findAll(Example.of(music));
    }


    @GetMapping("/top")
    public List<Music> top(@RequestParam(defaultValue = "20") int size) {
        return musicService.findTop(size);
    }
}
