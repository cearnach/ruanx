package com.ruanx.netease.controller;

import com.google.common.hash.BloomFilter;
import com.ruanx.netease.entity.Music;
import com.ruanx.netease.enumeration.IgnoreTypeEnum;
import com.ruanx.netease.service.IgnoreMusicItemService;
import com.ruanx.netease.service.MusicCrawlerService;
import com.ruanx.netease.service.PlaylistCrawlerService;
import com.ruanx.netease.service.UpdateMusicService;
import com.ruanx.netease.vo.ResultVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotBlank;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;

/**
 * @author Ruan Sheng
 * @date 2018/4/22 18:51
 */
@RestController
@RequestMapping("/crawler/music")
@Slf4j
public class MusicCrawlerController {
    private final IgnoreMusicItemService ignoreMusicItemService;
    private final MusicCrawlerService musicCrawlerService;
    private final UpdateMusicService updateMusicService;
    private final BloomFilter<String> bloomFilter;
    private final PlaylistCrawlerService playlistCrawlerService;
    private Comparator<Music> musicCommentComparator = (m1, m2) -> m2.getComment() - m1.getComment();

    public MusicCrawlerController(IgnoreMusicItemService ignoreMusicItemService,
                                  MusicCrawlerService musicCrawlerService,
                                  UpdateMusicService updateMusicService, @Qualifier(
            "EXCLUDE_MUSIC_ID_BLOOM_FILTER") BloomFilter<String> bloomFilter,
                                  PlaylistCrawlerService playlistCrawlerService) {
        this.ignoreMusicItemService = ignoreMusicItemService;
        this.musicCrawlerService = musicCrawlerService;
        this.updateMusicService = updateMusicService;
        this.bloomFilter = bloomFilter;
        this.playlistCrawlerService = playlistCrawlerService;
    }

    @GetMapping("/similar/{musicId}")
    public ResultVO<List<Music>> similarMusic(@NotBlank @PathVariable("musicId") String musicId,
                                              @RequestParam(value = "exclude", required = false) boolean exclude) throws Exception {
        log.info("根据音乐id获取相似歌曲");
        Set<Music> musicSet = musicCrawlerService.obtainSimilarMusicByMusicId(musicId);
        if (exclude) {
            musicSet = this.filterMusic(musicSet);
        }
        List<Music> list = musicSet.stream().sorted(musicCommentComparator).collect(Collectors.toList());
        return ResultVO.<List<Music>>builder().data(list).build();
    }

    @GetMapping("/parse/{musicId}")
    public Callable<String> parseUrl(@NotBlank @PathVariable("musicId") String musicId) {
        log.info("根据音乐id获取下载地址");
        return () -> musicCrawlerService.parseMusicUrl(musicId);

    }

    @GetMapping("/relevance/{musicId}")
    public ResultVO<List<Music>> musicInRelevancePlaylist(@PathVariable("musicId") String musicId,
                                                          @RequestParam(value = "exclude",
                                                                  required = false) boolean exclude) throws Exception {
        log.info("根据音乐id获取关联的歌单里面的音乐id集合");
        Set<Music> musicSet = musicCrawlerService.obtainMusicInfoInRelevancePlaylistByMusicId(musicId);
        if (exclude) {
            musicSet = this.filterMusic(musicSet);
        }
        List<Music> list = musicSet.stream().sorted(musicCommentComparator).collect(Collectors.toList());
        return ResultVO.<List<Music>>builder().data(list).build();
    }

    @GetMapping("/update/duration/{limit}")
    public Callable<Integer> updateByNullDuration(@PathVariable("limit") int limit) {
        log.info("更新播放时间为空的音乐数据");
        return () -> updateMusicService.updateNullDurationMusic(limit);
    }

    @GetMapping("/relevance/playlistId/{playlistId}")
    public Callable<List<Music>> musicInRelevancePlaylistAndSimilarMusic(@PathVariable("playlistId") String playlistId,
                                                                         @RequestParam(value = "exclude",
                                                                                 required = false) boolean exclude) {
        log.info("根据歌单id获取歌单里面的音乐集合,并根据音乐集合获取相似歌曲");
        return () -> {
            List<String> musicIdList = playlistCrawlerService.obtainMusicIdListByPlaylistId(playlistId);
            Set<Music> musicSet = musicCrawlerService.obtainSimilarMusicByMusicIdList(musicIdList);
            if (exclude) {
                musicSet = this.filterMusic(musicSet);
            }
            return musicSet.stream().sorted(musicCommentComparator).collect(Collectors.toList());
        };
    }

    private Set<Music> filterMusic(Set<Music> musicSet) {
        List<String> singers = ignoreMusicItemService.findAllByType(IgnoreTypeEnum.SINGER_NAME);
        return musicSet.stream()
                .filter(music -> !bloomFilter.mightContain(music.getId()))
                .filter(music -> singers != null && !singers.contains(music.getSinger()))
                .collect(Collectors.toSet());
    }
}
