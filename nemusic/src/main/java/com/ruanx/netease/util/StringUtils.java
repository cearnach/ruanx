package com.ruanx.netease.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @author 阮胜
 * @date 2018/9/30 13:27
 */
public class StringUtils extends org.springframework.util.StringUtils {

    public static final String COMMA = ",";

    /**
     * 在第一次出现分隔符之前获取子串,分隔符没有返回
     * <p>
     * 例如:
     * StringUtils.substringBefore(null, *)      = null
     * StringUtils.substringBefore("", *)        = ""
     * StringUtils.substringBefore("abc", "a")   = ""
     * StringUtils.substringBefore("abcba", "b") = "a"
     * StringUtils.substringBefore("abc", "c")   = "ab"
     * StringUtils.substringBefore("abc", "d")   = "abc"
     * StringUtils.substringBefore("abc", "")    = ""
     * StringUtils.substringBefore("abc", null)  = "abc"
     *
     * @param str       原始字符串
     * @param separator 要查找的字符串
     * @return 在第一次出现分离器之前的子串
     */
    public static String substringBefore(String str, String separator) {
        if (isEmpty(str) || separator == null) {
            return str;
        }
        if (separator.length() == 0) {
            return "";
        }
        int pos = str.indexOf(separator);
        if (pos == -1) {
            return str;
        }
        return str.substring(0, pos);
    }

    /**
     * 首字母转小写
     *
     * @param str
     * @return
     */
    public static String toLowerCaseFirstChar(String str) {
        if (isEmpty(str) || Character.isLowerCase(str.charAt(0))) {
            return str;
        } else {
            return Character.toLowerCase(str.charAt(0)) + str.substring(1);
        }
    }


    /**
     * 首字母转大写
     *
     * @param str
     * @return
     */
    public static String toUpperCaseFirstChar(String str) {
        if (isEmpty(str) || Character.isLowerCase(str.charAt(0))) {
            return str;
        } else {
            return Character.toUpperCase(str.charAt(0)) + str.substring(1);
        }
    }

    /**
     * 根据分隔符把string 转为 list 对象
     *
     * @param rawStr
     * @param separator
     * @return
     */
    public static List<String> delimitedStringToList(String rawStr, String separator) {
        if (isEmpty(rawStr)) {
            return null;
        }
        return Arrays.asList(rawStr.split(separator));
    }

    /**
     * 将字符串通过分隔符','分隔为集合
     *
     * @param rawStr
     * @return
     */
    public static List<String> commaDelimitedStringToList(String rawStr) {
        return delimitedStringToList(rawStr, COMMA);
    }

    public static String of(Object obj) {
        if (obj == null) {
            return "";
        }
        return String.valueOf(obj);
    }

    /**
     * 根据文件地址读入字符串
     *
     * @param filePath
     * @return
     */
    public static String readFromFile(String filePath) {
        try (FileReader fr = new FileReader(filePath);
             BufferedReader br = new BufferedReader(fr)
        ) {
            String line;
            StringBuilder sb = new StringBuilder();
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            return sb.toString();
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 根据文件读入字符串
     * @param file
     * @return
     */
    public static String readFromFile(File file) {
        return readFromFile(file.getAbsolutePath());
    }

    /**
     * 包含格式换行符等
     * @param fileName
     * @return
     */
    public static String readFromFileRaw(String fileName) {
        String encoding = "UTF-8";
        File file = new File(fileName);
        Long filelength = file.length();
        byte[] filecontent = new byte[filelength.intValue()];
        try {
            FileInputStream in = new FileInputStream(file);
            in.read(filecontent);
            in.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            return new String(filecontent, encoding);
        } catch (UnsupportedEncodingException e) {
            System.err.println("The OS does not support " + encoding);
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 将Map转换为Http参数格式
     * @param paramMap
     * @return
     */
    public static String toParamString(Map<String, String> paramMap) {
        if (paramMap == null || paramMap.size() == 0) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        int size = paramMap.size();
        int index = 0;
        for (String key : paramMap.keySet()) {
            String value = paramMap.get(key);
            sb.append(key);
            sb.append("=");
            sb.append(value);
            if (++index != size) {
                sb.append("&");
            }
        }
        return sb.toString();
    }

    public static void writeToFile(String str, String fileName) throws IOException {
        File file = new File(fileName);
        if (!file.exists()) {
            boolean mkdirs = file.getParentFile().mkdirs();
            if (mkdirs && !file.createNewFile()) {
                System.out.println("创建文件失败");
            }
        }
        try (FileOutputStream fos = new FileOutputStream(fileName)) {
            fos.write(str.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
