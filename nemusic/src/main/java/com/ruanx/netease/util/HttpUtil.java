package com.ruanx.netease.util;

import com.ruanx.netease.bean.GlobalProxy;
import lombok.extern.slf4j.Slf4j;
import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author Ruan Sheng
 * @date 2018/4/20 18:19
 */
@Slf4j
public class HttpUtil {

    private static final int MAX_TIME_OUT = 3;
    private static final AtomicInteger CONNECT_TIME_OUT_COUNT = new AtomicInteger();

    public static Response get(Request req) {
        OkHttpClient http = new OkHttpClient();

        Call call = http.newCall(req);
        try {
            return call.execute();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String get(String url) {
        Request req = new Request.Builder().url(url).build();
        try {
            return get(req).body().string();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }

    public static String post(String url, Map<String, String> dataMap) throws IOException {
        Set<String> set = dataMap.keySet();
        FormBody.Builder builder = new FormBody.Builder();
        for (String key : set) {
            String value = dataMap.get(key);
            builder.addEncoded(key, value);
        }
        FormBody body = builder.build();
        Request req = new Request.Builder()
                .url(url)
                .post(body)
                .addHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:17.0) Gecko/20100101 " +
                        "Firefox/17.0")
                .addHeader("Content-Type", "application/x-www-form-urlencoded")
                .build();
        Response resp = post(req);
        if (resp == null || resp.body() == null) {
            return "";
        }
        String result = resp.body().string();
        if (StringUtils.isEmpty(result)) {
            result = resp.headers().toString();
        }
        return result;
    }

    public static Response post(Request req) {
        OkHttpClient okHttpClient;
        if (StringUtils.isEmpty(GlobalProxy.HOST)) {
            okHttpClient = new OkHttpClient();
        } else {
            okHttpClient = new OkHttpClient.Builder()
                    .proxy(new Proxy(Proxy.Type.HTTP, new InetSocketAddress(GlobalProxy.HOST,
                            GlobalProxy.PORT)))
                    .build();
        }

        Call call = okHttpClient.newCall(req);
        try {
            Response res = call.execute();
            CONNECT_TIME_OUT_COUNT.set(0);
            return res;
        } catch (IOException e) {
            log.info("HttpUtil 执行异常 , 详情:{}", e.getMessage());
            if (CONNECT_TIME_OUT_COUNT.getAndIncrement() == MAX_TIME_OUT) {
                GlobalProxy.HOST = "";
            }
            return null;

        }
    }

    public static Map<String, String> parseCookie(String cookieStr) {
        try {

            String[] cookies = cookieStr.split(";");
            HashMap<String, String> cookieMap = new HashMap<>(cookies.length);
            for (String cookie : cookies) {
                int firstIndex = cookie.indexOf("=");
                String key = cookie.substring(0, firstIndex);
                String value = cookie.substring(firstIndex + 1);
                cookieMap.put(key, value);
            }
            return cookieMap;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
