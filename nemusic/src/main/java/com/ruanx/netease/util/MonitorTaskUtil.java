package com.ruanx.netease.util;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.concurrent.BasicThreadFactory;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 监听多线程进度
 *
 * @author Ruan Sheng
 * @date 2018/4/19 18:44
 */
@Slf4j
public class MonitorTaskUtil {
    /**
     * 监听器核心线程池数量
     */
    private static final int CORE_POOL_SIZE = 10;
    /**
     * 延迟1秒启动(时间单位以TimeUnit为准,默认TimeUnit.SECONDS)
     */
    private static final int INITIAL_DELAY = 1;
    /**
     * 频率周期
     */
    private static final int PERIOD = 1;
    private static ScheduledExecutorService executorService;

    static {
        executorService = new ScheduledThreadPoolExecutor(CORE_POOL_SIZE,
                new BasicThreadFactory.Builder().namingPattern("爬虫监听线程ID:%d").daemon(true).build());
    }


    /**
     * 监听爬虫进度
     *
     * @param latch       多线程 CountDownLatch
     * @param description 描述操作信息,比如:"获取音乐信息"
     * @param itemCount   项目数量
     */
    public static ScheduledFuture<?> monitorCrawler(CountDownLatch latch, String description, int itemCount) {
        log.info("启动爬虫任务监听线程");
        return executorService.scheduleAtFixedRate(() -> {
            long currentLatchCount = latch.getCount();
            long finishedCount = itemCount - currentLatchCount;
            String printFormatterStr = description.concat(" 已完成:%d , 总任务数:%d , 完成百分比%.2f%%");
            printMonitorTaskLog(printFormatterStr, finishedCount, itemCount);
        }, INITIAL_DELAY, PERIOD, TimeUnit.SECONDS);
    }

    /**
     * 自定义输出日志,由于爬虫线程结束后,该监听线程会自动关闭(会导致无法输出完成后的信息)
     * 所以在爬虫线程处理完毕后,需要在爬虫线程手动再次调用,输出完成信息
     *
     * @param printFormatterStr
     * @param finishedCount
     * @param itemCount
     * @return
     */
    public static void printMonitorTaskLog(String printFormatterStr, long finishedCount, int itemCount) {
        String printStr = String.format(printFormatterStr, finishedCount, itemCount,
                (double) finishedCount / itemCount * 100);
        log.info(printStr);
    }
}
