package com.ruanx.netease.util;

import com.ruanx.netease.exception.MusicUrlParserException;

/**
 * @author Ruan Sheng
 * @date 2018/4/24 18:45
 */
public class MusicUtil {
    /**
     * 取音乐文件的长度
     *
     * @param size 文件大小
     * @param br   编码率
     * @return 音乐文件长度
     */
    private static int obtainMusicLength(int size, int br) {
        return size * 8 / br;
    }

    /**
     * 把音乐长度转为时间格式
     *
     * @param size
     * @param br
     * @return
     * @throws MusicUrlParserException
     */
    public static String formatMusicLength(int size, int br) throws MusicUrlParserException {
        if (size == 0 || br == 0) {
            throw new MusicUrlParserException();
        }
        int musicLength = obtainMusicLength(size, br);
        int minute = musicLength / 60;
        int second = musicLength % 60;
        String minuteStr = minute < 10 ? "0" + minute : "" + minute;
        String secondStr = second < 10 ? "0" + second : "" + second;
        return String.format("%s:%s", minuteStr, secondStr);
    }
}
