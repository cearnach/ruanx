package com.ruanx.netease.util;

import java.util.HashMap;
import java.util.Map;

/**
 * 中文转数字
 *
 * @author Ruan Sheng
 * @date 2018/4/19 14:22
 */
public class NumberConverterUtil {
    private static final Map<String, String> UNIT_MAP;

    static {
        UNIT_MAP = new HashMap<>();
        UNIT_MAP.put("千", "000");
        UNIT_MAP.put("万", "0000");
        UNIT_MAP.put("百万", "000000");
        UNIT_MAP.put("千万", "0000000");
    }

    /**
     * 中文转数字
     *
     * @param number 要转换的数值(例如: 12万)
     * @return
     */
    public static Integer convert(String number) {
        return UNIT_MAP.keySet()
                .stream()
                .filter(number::contains)
                .map(key -> Integer.valueOf(number.replace(key, UNIT_MAP.get(key))))
                .findFirst()
                .orElse(0);
    }
}
