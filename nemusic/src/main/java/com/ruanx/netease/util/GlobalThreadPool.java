package com.ruanx.netease.util;

import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author Ruan Sheng
 * @date 2018/4/20 18:47
 */
public final class GlobalThreadPool {
    /**
     * 默认开启线程数
     */
    private static final int CORE_POOL_SIZE = 5;
    /**
     * 线程池最大数量
     */
    private static final int MAX_POOL_SIZE = 20;

    /**
     * 线程闲置存活时间
     */
    private static final int KEEP_ALIVE_TIME = 10;
    /**
     * 内部维护的线程池
     */
    public static ThreadPoolExecutor POOL;


    static {
        POOL = new ThreadPoolExecutor(CORE_POOL_SIZE, MAX_POOL_SIZE, KEEP_ALIVE_TIME, TimeUnit.MINUTES
                , new LinkedBlockingDeque<>(), (ThreadFactory) Thread::new);
    }
}
