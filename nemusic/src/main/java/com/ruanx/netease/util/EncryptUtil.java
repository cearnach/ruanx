package com.ruanx.netease.util;

import jdk.nashorn.api.scripting.ScriptObjectMirror;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Hex;
import org.springframework.core.io.ClassPathResource;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Ruan Sheng
 * @date 2018/4/20 19:15
 */
@Slf4j
public class EncryptUtil {
    private static final String MODULUS =
            "00e0b509f6259df8642dbc35662901477df22677ec152b5ff68ace615bb7b725152b3ab17a876aea8a5aa76d2e417629ec4ee341f56135fccf695280104e0312ecbda92557c93870114af6c9d05c4f7f0c3685b7a46bee255932575cce10b424d813cfe4875d3e82047b97ddef52741d546b8e289dc6935b3ece0462db0a22b8e7";
    private static final String NONCE = "0CoJUm6Qyw8W8jud";
    private static final String PUB_KEY = "010001";
    private static final String SEC_KEY = "cd859f54539b24b7";

    private static Invocable invoke;

    static {
        try {
            long l = System.currentTimeMillis();
            ScriptEngineManager manager = new ScriptEngineManager();
            ScriptEngine engine = manager.getEngineByName("js");
            ClassPathResource resource = new ClassPathResource("/static/ne/js/ne.js");
            InputStream in = resource.getInputStream();
            InputStreamReader inputStreamReader = new InputStreamReader(in);
            engine.eval(inputStreamReader);
            invoke = (Invocable) engine;
            log.info("初始化加密js文件,耗时:{}ms", System.currentTimeMillis() - l);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Map<String, String> encryptByJs(String encryptText) throws ScriptException,
            NoSuchMethodException, InterruptedException {
        Object o;
        synchronized (EncryptUtil.class) {
            o = invoke.invokeFunction("d", encryptText, PUB_KEY, MODULUS, NONCE);
        }
        String params = ((ScriptObjectMirror) o).get("encText").toString();
        String encSecKey = ((ScriptObjectMirror) o).get("encSecKey").toString();
        HashMap<String, String> map = new HashMap<>(2);
        map.put("params", params);
        map.put("encSecKey", encSecKey);
        return map;
    }

    /**
     * AES加密
     *
     * @param encText
     * @param secKey
     * @return
     * @throws Exception
     */
    private static String encrypt(String encText, String secKey) throws Exception {
        byte[] raw = secKey.getBytes();
        SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
        // "算法/模式/补码方式"
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        // 使用CBC模式，需要一个向量iv，可增加加密算法的强度
        IvParameterSpec iv = new IvParameterSpec("0102030405060708".getBytes());
        cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);
        byte[] encrypted = cipher.doFinal(encText.getBytes());
        return Base64.getEncoder().encodeToString(encrypted);
    }

    /**
     * 字符填充
     *
     * @param result
     * @param n
     * @return
     */
    private static String zFill(String result, int n) {
        if (result.length() >= n) {
            result = result.substring(result.length() - n);
        } else {
            StringBuilder sb = new StringBuilder();
            for (int i = n; i > result.length(); i--) {
                sb.append("0");
            }
            sb.append(result);
            result = sb.toString();
        }
        return result;
    }


    /**
     * 要加密的字符串
     *
     * @param encText
     * @return
     * @throws Exception
     */
    public static Map<String, String> encryptParamsAndEncSecKey(String encText) throws Exception {
        //私钥，随机16位字符串（自己可改）
        // String text = "{\"ids\":\"[540968]\",\"br\":128000,\"csrf_token\":\"\"}";
        //String text = "{\"username\": \"\", \"rememberLogin\": \"true\", \"password\": \"\"}";
        //2次AES加密，得到params
        String params = EncryptUtil.encrypt(EncryptUtil.encrypt(encText, NONCE), SEC_KEY);
        StringBuilder stringBuffer = new StringBuilder(SEC_KEY);
        //逆置私钥
        String newSecKey = stringBuffer.reverse().toString();
        String hex = new String(Hex.encodeHex(newSecKey.getBytes()));
        BigInteger bigInteger1 = new BigInteger(hex, 16);
        BigInteger bigInteger2 = new BigInteger(PUB_KEY, 16);
        BigInteger bigInteger3 = new BigInteger(MODULUS, 16);
        //RSA加密计算
        BigInteger bigInteger4 = bigInteger1.pow(bigInteger2.intValue()).remainder(bigInteger3);
        String encSecKey = new String(Hex.encodeHex(bigInteger4.toByteArray()));
        //字符填充
        encSecKey = EncryptUtil.zFill(encSecKey, 256);
        HashMap<String, String> map = new HashMap<>(2);
        map.put("params", params);
        map.put("encSecKey", encSecKey);
        return map;

    }

}

