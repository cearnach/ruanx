package com.ruanx.netease.util;

import com.ruanx.netease.bean.GlobalProxy;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * Jsoup工具类
 *
 * @author Ruan Sheng
 * @date 2018/4/19 17:26
 */

public class JsoupUtil {
    /**
     * 使用代理创建Jsoup连接
     *
     * @param url url
     * @return 代理后创建的Jsoup连接
     */
    public static Connection connect(String url) {
        if (StringUtils.isEmpty(GlobalProxy.HOST)) {
            return Jsoup.connect(url);
        }
        GlobalProxy.checkValid();
        return Jsoup.connect(url).proxy(GlobalProxy.HOST, GlobalProxy.PORT);
    }

    public static Connection connectIgnoreProxy(String url) {
        return Jsoup.connect(url);
    }

    public static void main(String[] args) {
        int[] data = new int[1000000000];
        Random r = new Random();
        for (int i = 0; i < data.length; i++) {
            data[i] = r.nextInt(1000000);
        }
        int size = 5;
        long calc1Start = System.currentTimeMillis();
        List<Integer[]> list = calc1(data, size);
        long calc1End = System.currentTimeMillis();
        long calc2Start = System.currentTimeMillis();
        List<Integer[]> list2 = calc2(data, size);
        long calc2End = System.currentTimeMillis();

        System.out.println(list.size());
        System.out.println(list2.size());

        for (int i = 0; i < list.size(); i++) {
            Integer[] arr1 = list.get(i);
            Integer[] arr2 = list2.get(i);
            boolean equals = Arrays.equals(arr1, arr2);
            if (!equals) {
                System.out.println("not eq");
                break;
            }
        }

        System.out.println("eq");

        System.out.println("calc1 time :" + (calc1End - calc1Start));
        System.out.println("calc2 time :" + (calc2End - calc2Start));
    }

    private static List<Integer[]> calc1(int[] data, int size) {
        int count = 0;
        List<Integer[]> list = new ArrayList<>();
        for (int i = 0; i < data.length; i++) {
            if (i == data.length - size) {
                break;
            }
            Integer[] tmp = new Integer[size];
            for (int j = 1; j <= size; j++) {
                count++;
                tmp[size - j] = data[i + j + 1];
                if (data[i + j] >= data[i + j + 1]) {
                    break;
                }
                if (j == size) {
                    list.add(tmp);
                }
            }
        }
        System.out.println("calc1 times:" + count);
        return list;
    }

    private static List<Integer[]> calc2(int[] data, int size) {
        int count = 0;
        int continuous = 0;
        List<Integer[]> list = new ArrayList<>();
        for (int i = 0; i < data.length; i++) {
            if (i == data.length - size) {
                break;
            }
            if (data[i] < data[i + 1]) {
                int diff = ++continuous - size;
                if (diff >= 0) {
                    Integer[] tmp = new Integer[size];
                    for (int j = 0; j < size; j++) {
                        tmp[j] = data[i + 1 - j];
                    }
                    list.add(tmp);
                }
            } else {
                continuous = 0;
            }
        }
        return list;
    }
}
