package com.ruanx.netease.util;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Ruan Sheng
 * @date 2018/4/19 14:25
 */
public class RegexUtil {
    /**
     * 正则匹配
     *
     * @param str     源文本
     * @param pattern 匹配表达式
     * @param groupId 组Id,默认为0,0为返回整个匹配到的文本,1个括号代表一个组
     * @return
     */
    public static List<String> regexList(String str, String pattern, int groupId) {
        List<String> strList = new ArrayList<>();
        Matcher matcher = regexMather(str, pattern);
        while (matcher.find()) {
            strList.add(matcher.group(groupId));
        }
        return strList;
    }

    public static Matcher regexMather(String str, String pattern) {
        return Pattern.compile(pattern).matcher(str);
    }

    public static List<String> regexList(String str, String pattern) {
        return regexList(str, pattern, 0);

    }
}
