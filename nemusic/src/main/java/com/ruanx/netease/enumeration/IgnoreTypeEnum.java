package com.ruanx.netease.enumeration;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * <p>标题: </p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2019</p>
 * <p>公司: 智业软件股份有限公司</p>
 *
 * @version: 1.0
 * @author: ruansheng
 * @date 5/13/2019
 */
@Getter
@AllArgsConstructor
public enum IgnoreTypeEnum {
    /**
     * CODE = 代号 , NAME = 状态名称
     */
    MUSIC_ID(1, "音乐ID"),
    PLAYLIST_ID(2, "歌单ID"),
    SINGER_NAME(3, "歌手名称");

    private Integer code;
    private String name;

    public static IgnoreTypeEnum findByCode(Integer code) {
        for (IgnoreTypeEnum sellerStatusEnum : IgnoreTypeEnum.values()) {
            if (sellerStatusEnum.getCode().equals(code)) {
                return sellerStatusEnum;
            }
        }
        return null;
    }
}
