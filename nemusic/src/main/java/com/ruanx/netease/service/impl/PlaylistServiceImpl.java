package com.ruanx.netease.service.impl;

import com.ruanx.netease.entity.Playlist;
import com.ruanx.netease.repository.PlaylistRepository;
import com.ruanx.netease.service.PlaylistService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * @author Ruan Sheng
 * @date 2018/4/23 19:43
 */
@Service
@Transactional(rollbackFor = Exception.class)
@Slf4j
public class PlaylistServiceImpl implements PlaylistService {
    private final PlaylistRepository playlistDao;

    public PlaylistServiceImpl(PlaylistRepository playlistDao) {
        this.playlistDao = playlistDao;
    }

    @Override
    public Optional<Playlist> findById(String playlistId) {
        return playlistDao.findById(playlistId);
    }

    @Override
    public Playlist save(Playlist playlist) {
        log.info("保存歌单 : {}", playlist);
        return playlistDao.save(playlist);
    }

}
