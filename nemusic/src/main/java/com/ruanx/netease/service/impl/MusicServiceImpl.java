package com.ruanx.netease.service.impl;

import com.ruanx.netease.aop.OnMusicSave;
import com.ruanx.netease.entity.Music;
import com.ruanx.netease.exception.MusicNotFoundException;
import com.ruanx.netease.repository.MusicRepository;
import com.ruanx.netease.service.MusicService;
import com.ruanx.netease.service.base.BaseServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * @author Ruan Sheng
 * @date 2018/4/19 23:09
 */
@Service
@Slf4j
public class MusicServiceImpl extends BaseServiceImpl<Music, String, MusicRepository> implements MusicService {

    public static final int SINGER_COLUMN_LENGTH = 254;

    @OnMusicSave
    @Override
    public Music save(Music music) {
        if (music.getSinger() != null && music.getSinger().length() > SINGER_COLUMN_LENGTH) {
            music.setSinger(music.getSinger().substring(0, SINGER_COLUMN_LENGTH));
        }
        log.info("【 保存音乐 】 , 歌名:{} , 歌手:{} , 评论数:{}", music.getTitle(), music.getSinger(),
                music.getComment());
        return repository.save(music);
    }

    @Override
    public Music update(Music music) throws MusicNotFoundException {
        log.info("更新音乐 : {}", music);
        if (music == null || StringUtils.isEmpty(music.getId())) {
            return null;
        }
        return repository.findById(music.getId()).orElseThrow(MusicNotFoundException::new);
    }

    @Override
    public List<String> findAllId(Pageable pageable) {
        return repository.findAllId(pageable);
    }

    @Override
    public List<String> findIdByCommentLess(int comment, Pageable pageable) {
        return repository.findIdByCommentLess(comment, pageable);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateComment(String musicId, int comment) {
        if (comment < 0) {
            return 0;
        }
        return repository.updateComment(musicId, comment);
    }

    @Override
    public List<String> findIdByUpdateTime(Pageable pageable) {
        return repository.findIdByUpdateTime(pageable);
    }

    @Override
    public List<Music> findTop(int size) {
        return repository.findTop(size);
    }

    @Override
    public Page<Music> findAll(Pageable pageable) {
        return repository.findAll(pageable);
    }

    @Override
    public void delete(String musicId) throws Exception {
        repository.deleteById(musicId);
    }
}
