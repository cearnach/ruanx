package com.ruanx.netease.service;

import com.ruanx.netease.entity.Playlist;

import java.util.Optional;

/**
 * @author Ruan Sheng
 * @date 2018/4/23 19:42
 */
public interface PlaylistService {
    Optional<Playlist> findById(String playlistId);

    Playlist save(Playlist playlist);
}
