package com.ruanx.netease.service.impl;

import com.ruanx.netease.crawler.MusicCrawler;
import com.ruanx.netease.entity.Music;
import com.ruanx.netease.repository.MusicRepository;
import com.ruanx.netease.service.UpdateMusicService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Ruan Sheng
 * @date 2018/4/24 19:32
 */
@Service
@Transactional(rollbackFor = Exception.class)
@Slf4j
public class UpdateMusicServiceImpl implements UpdateMusicService {
    private final MusicRepository musicDao;

    public UpdateMusicServiceImpl(MusicRepository musicDao) {
        this.musicDao = musicDao;
    }

    /**
     * 更新所有时间为空的音乐
     */
    @Override
    public int updateNullDurationMusic(int limit) throws InterruptedException {
        final List<Music> musicList = musicDao.findAllByNullDuration(limit);
        List<String> idList = musicList.stream().map(Music::getId).collect(Collectors.toList());
        Map<String, String> durationMap = MusicCrawler.obtainMusicDurationListByMusicIdList(idList);
        musicList.stream()
                .filter(music -> durationMap.containsKey(music.getId()))
                .forEach(music -> {
                    music.setDuration(durationMap.get(music.getId()));
                    Music m = musicDao.save(music);
                    log.info("更新音乐时间 : {}", m);
                });
        return durationMap.size();
    }

}
