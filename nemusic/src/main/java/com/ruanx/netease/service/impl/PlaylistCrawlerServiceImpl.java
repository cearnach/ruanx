package com.ruanx.netease.service.impl;

import com.ruanx.netease.crawler.PlaylistCrawler;
import com.ruanx.netease.entity.Music;
import com.ruanx.netease.entity.Playlist;
import com.ruanx.netease.exception.MusicCrawlerTimeOutException;
import com.ruanx.netease.service.MusicCrawlerService;
import com.ruanx.netease.service.PlaylistCrawlerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * @Author: Ruan Sheng
 * @Date: 2018/4/23 14:27
 */
@Slf4j
@Service
@Transactional(rollbackFor = Exception.class)
public class PlaylistCrawlerServiceImpl implements PlaylistCrawlerService {

    /**
     * 避免循环依赖
     */
    @Autowired
    private MusicCrawlerService musicCrawlerService;

    @Override
    public List<Playlist> obtainRelevancePlaylistInfoListByPlaylistId(String playlistId) throws IOException
            , InterruptedException {
        return PlaylistCrawler.obtainRelevancePlaylistInfoListByPlayListId(playlistId);
    }

    @Override
    public Set<Music> obtainMusicInfoSetByPlaylistId(String playlistId) throws InterruptedException,
            IOException, MusicCrawlerTimeOutException {
        if (StringUtils.isEmpty(playlistId)) {
            return Collections.emptySet();
        }
        List<String> musicIdList = PlaylistCrawler.obtainMusicIdListByPlaylistId(playlistId);
        return musicCrawlerService.obtainMusicInfoByMusicIdList(musicIdList);
    }

    @Override
    public List<Playlist> obtainPlaylistInfoListByPlaylistIdList(List<String> playlistIdList) throws InterruptedException {
        return PlaylistCrawler.obtainPlaylistInfoListByPlaylistIdList(playlistIdList);
    }

    /**
     * 根据歌单id获取歌单里面的音乐id集合
     *
     * @param playlistId
     * @return
     * @throws IOException
     */
    @Override
    public List<String> obtainMusicIdListByPlaylistId(String playlistId) throws IOException {
        return PlaylistCrawler.obtainMusicIdListByPlaylistId(playlistId);
    }

    @Override
    public Set<Music> obtainMusicInfoSetByPlaylistIdList(List<String> playlistIdList) throws InterruptedException, MusicCrawlerTimeOutException {
        List<String> musicIdList = PlaylistCrawler.obtainMusicIdListByPlaylistIdList(playlistIdList);
        return musicCrawlerService.obtainMusicInfoByMusicIdList(musicIdList);
    }
}
