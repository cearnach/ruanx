package com.ruanx.netease.service;

import com.ruanx.netease.entity.Music;
import com.ruanx.netease.entity.Playlist;
import com.ruanx.netease.exception.MusicCrawlerTimeOutException;
import com.ruanx.netease.exception.MusicMaxParseCountOutBoundsException;

import java.io.IOException;
import java.util.List;
import java.util.Set;

/**
 * @Author: Ruan Sheng
 * @Date: 2018/4/23 14:26
 */
public interface PlaylistCrawlerService {
    List<Playlist> obtainRelevancePlaylistInfoListByPlaylistId(String playlistId) throws IOException,
            InterruptedException;

    Set<Music> obtainMusicInfoSetByPlaylistId(String playlistId) throws InterruptedException,
            MusicMaxParseCountOutBoundsException, IOException, MusicCrawlerTimeOutException;

    List<Playlist> obtainPlaylistInfoListByPlaylistIdList(List<String> playlistIdList) throws InterruptedException;

    List<String> obtainMusicIdListByPlaylistId(String playlistId) throws IOException;

    Set<Music> obtainMusicInfoSetByPlaylistIdList(List<String> playlistIdList) throws InterruptedException,
            MusicCrawlerTimeOutException;
}
