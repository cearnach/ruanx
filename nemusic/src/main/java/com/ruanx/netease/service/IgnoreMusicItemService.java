package com.ruanx.netease.service;

import com.ruanx.netease.entity.IgnoreMusicItem;
import com.ruanx.netease.entity.IgnoreMusicItemId;
import com.ruanx.netease.enumeration.IgnoreTypeEnum;
import com.ruanx.netease.service.base.BaseService;

import java.util.List;

/**
 * <p>标题: </p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2019</p>
 * <p>公司: 智业软件股份有限公司</p>
 *
 * @version: 1.0
 * @author: ruansheng
 * @date 5/13/2019
 */
public interface IgnoreMusicItemService extends BaseService<IgnoreMusicItem, IgnoreMusicItemId> {
    List<String> findAllByType(IgnoreTypeEnum musicId);
}
