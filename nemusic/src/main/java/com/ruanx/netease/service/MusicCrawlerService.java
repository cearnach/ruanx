package com.ruanx.netease.service;

import com.ruanx.netease.entity.Music;
import com.ruanx.netease.exception.MusicCrawlerTimeOutException;
import com.ruanx.netease.exception.MusicNotFoundException;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * @author Ruan Sheng
 * @date 2018/4/22 11:28
 */
public interface MusicCrawlerService {
    /**
     * 根据音乐id获取相似音乐
     *
     * @param musicId 音乐id
     * @return
     * @throws IOException
     * @throws InterruptedException
     * @throws MusicCrawlerTimeOutException
     */
    Set<Music> obtainSimilarMusicByMusicId(String musicId) throws Exception;

    Set<Music> obtainSimilarMusicByMusicIdList(List<String> musicIdList) throws MusicCrawlerTimeOutException, InterruptedException;

    /**
     * 如果要获取的相似音乐id在redis里面已经存在,则直接在数据库里面获取
     *
     * @param musicIdList
     * @return
     */
    Set<Music> pullMusicInfoFromDb(Collection<String> musicIdList);

    /**
     * 将新获取到的音乐信息更新到redis和数据库
     *
     * @param musicSet
     */
    void pushDataToRedisAndDb(Set<Music> musicSet);

    /**
     * 根据音乐id解析音乐地址
     * @param musicId
     * @return
     * @throws Exception
     */
    String parseMusicUrl(String musicId) throws Exception;

    /**
     * 根据音乐id获取关联歌单id集合
     * @param musicId
     * @return
     * @throws InterruptedException
     * @throws MusicCrawlerTimeOutException
     * @throws IOException
     */
    List<String> obtainRelevancePlaylistIdList(String musicId) throws Exception;

    /**
     * 根据音乐id获取音乐信息
     * @param musicId
     * @return
     * @throws MusicNotFoundException
     * @throws IOException
     */
    Music obtainMusicInfoByMusicId(String musicId) throws Exception;

    /**
     * 根据音乐id集合获取音乐集合
     * @param similarMusicIdList
     * @return
     * @throws InterruptedException
     * @throws MusicCrawlerTimeOutException
     */
    Set<Music> obtainMusicInfoByMusicIdList(List<String> similarMusicIdList) throws InterruptedException,
            MusicCrawlerTimeOutException;

    /**
     * 根据音乐id获取关联列表并且再次分析关联列表获取所有列表的音乐信息集合
     * @param musicId
     * @return
     * @throws IOException
     * @throws MusicCrawlerTimeOutException
     * @throws InterruptedException
     */
    Set<Music> obtainMusicInfoInRelevancePlaylistByMusicId(String musicId) throws Exception;
}
