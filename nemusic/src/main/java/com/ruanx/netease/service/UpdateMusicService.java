package com.ruanx.netease.service;

/**
 * @author Ruan Sheng
 * @date 2018/4/24 19:31
 */
public interface UpdateMusicService {
    int updateNullDurationMusic(int limit) throws InterruptedException;
}
