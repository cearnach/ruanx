package com.ruanx.netease.service.impl;

import com.ruanx.netease.entity.IgnoreMusicItem;
import com.ruanx.netease.entity.IgnoreMusicItemId;
import com.ruanx.netease.enumeration.IgnoreTypeEnum;
import com.ruanx.netease.repository.IgnoreMusicItemRepository;
import com.ruanx.netease.service.IgnoreMusicItemService;
import com.ruanx.netease.service.base.BaseServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>标题: </p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2019</p>
 * <p>公司: 智业软件股份有限公司</p>
 *
 * @version: 1.0
 * @author: ruansheng
 * @date 5/13/2019
 */
@Service
public class IgnoreMusicItemServiceImpl extends BaseServiceImpl<IgnoreMusicItem, IgnoreMusicItemId, IgnoreMusicItemRepository>
        implements IgnoreMusicItemService {
    @Override
    public List<String> findAllByType(IgnoreTypeEnum musicId) {
        return repository.findAllByType(musicId.getCode());
    }
}
