package com.ruanx.netease.service.impl;

import com.ruanx.netease.crawler.MusicCrawler;
import com.ruanx.netease.entity.Music;
import com.ruanx.netease.exception.MusicCrawlerTimeOutException;
import com.ruanx.netease.filter.NeFilter;
import com.ruanx.netease.service.MusicCrawlerService;
import com.ruanx.netease.service.MusicService;
import com.ruanx.netease.service.PlaylistCrawlerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Ruan Sheng
 * @date 2018/4/22 11:31
 */
@Service
@Slf4j
@Transactional(rollbackFor = Exception.class)
public class MusicCrawlerServiceImpl implements MusicCrawlerService {
    private final MusicService musicService;
    private final NeFilter neFilter;


    /**
     * 避免循环依赖
     */
    @Autowired
    private PlaylistCrawlerService playlistCrawlerService;

    public MusicCrawlerServiceImpl(MusicService musicService, NeFilter neFilter) {
        this.musicService = musicService;
        this.neFilter = neFilter;
    }


    @Override
    public Music obtainMusicInfoByMusicId(String musicId) throws Exception {
        return musicService.findByIdOptional(musicId).orElse(MusicCrawler.obtainMusicInfoById(musicId));
    }

    @Override
    public Set<Music> obtainMusicInfoByMusicIdList(List<String> musicIdList)
            throws MusicCrawlerTimeOutException, InterruptedException {
        if (CollectionUtils.isEmpty(musicIdList)) {
            return Collections.emptySet();
        }

        // 检查并对是否已经存在的id进行分类
        List<String> existsIdList = neFilter.existsByMusicIdBatch(musicIdList);
        Set<String> existsIdSet = new HashSet<>(existsIdList);
        // 找出所有不存在的音乐ID
        Set<String> notExitsIdSet = musicIdList.stream()
                .filter(id -> !existsIdSet.contains(id))
                .collect(Collectors.toSet());
        // 从数据库获取已经存在的音乐信息
        Set<Music> musicSet = pullMusicInfoFromDb(existsIdList);
        // 音乐信息在数据库里面不存在,使用爬虫在线获取.
        Set<Music> nonexistenceMusicSet = MusicCrawler.obtainMusicInfoByMusicIdList(notExitsIdSet);
        // 把不存在的数据推送到数据库进行持久化
        pushDataToRedisAndDb(nonexistenceMusicSet);
        // 合并2个集合,得到完整的音乐信息集合
        musicSet.addAll(nonexistenceMusicSet);
        return musicSet;
    }

    @Override
    public Set<Music> obtainSimilarMusicByMusicId(String musicId) throws Exception {
        if (StringUtils.isEmpty(musicId)) {
            return Collections.emptySet();
        }
        //根据音乐id获取相似音乐id集合
        List<String> similarMusicIdList = MusicCrawler.obtainSimilarMusicIdListByMusicId(musicId);
        return obtainMusicInfoByMusicIdList(similarMusicIdList);
    }

    @Override
    public Set<Music> obtainSimilarMusicByMusicIdList(List<String> musicIdList) throws MusicCrawlerTimeOutException,
            InterruptedException {
        List<String> similarMusicIdList = MusicCrawler.obtainSimilarMusicIdListByMusicIdList(musicIdList);
        return obtainMusicInfoByMusicIdList(similarMusicIdList);
    }

    @Override
    public Set<Music> pullMusicInfoFromDb(Collection<String> musicIdList) {
        return musicIdList.stream()
                //获取音乐信息
                .map(id -> musicService.findByIdOptional(id).orElse(null))
                //过滤无效的音乐信息
                .filter(Objects::nonNull)
                //收集有效的音乐信息结果集
                .collect(Collectors.toSet());
    }


    @Override
    public void pushDataToRedisAndDb(Set<Music> musicSet) {
        musicSet.stream()
                //过滤无效音乐
                .filter(music -> !StringUtils.isEmpty(music.getId()))
                .filter(music -> music.getComment() > 0)
                .forEach(music -> {
                    try {
                        //对音乐信息进行持久化
                        musicService.save(music);
                    } catch (Exception e) {
                        log.error(e.getMessage());
                    }
                });
    }


    @Override
    public String parseMusicUrl(String musicId) throws Exception {
        return MusicCrawler.obtainMusicUrl(musicId);
    }

    @Override
    public List<String> obtainRelevancePlaylistIdList(String musicId) throws Exception {
        return MusicCrawler.obtainRelevancePlaylistIdList(musicId);

    }

    @Override
    public Set<Music> obtainMusicInfoInRelevancePlaylistByMusicId(String musicId) throws Exception {
        List<String> relevancePlaylistIdList = obtainRelevancePlaylistIdList(musicId);
        return playlistCrawlerService.obtainMusicInfoSetByPlaylistIdList(relevancePlaylistIdList);

    }
}
