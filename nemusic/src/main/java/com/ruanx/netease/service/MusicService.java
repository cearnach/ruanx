package com.ruanx.netease.service;

import com.ruanx.netease.entity.Music;
import com.ruanx.netease.exception.MusicNotFoundException;
import com.ruanx.netease.service.base.BaseService;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * @author Ruan Sheng
 * @date 2018/4/19 23:09
 */
public interface MusicService extends BaseService<Music, String> {
    Music update(Music music) throws MusicNotFoundException;

    List<String> findAllId(Pageable pageable);

    List<String> findIdByCommentLess(int comment, Pageable pageable);

    int updateComment(String musicId, int comment);

    List<String> findIdByUpdateTime(Pageable pageable);

    List<Music> findTop(int size);
}
