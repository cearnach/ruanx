package com.ruanx.netease.property;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * <p>标题: </p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2019</p>
 * <p>公司: 智业软件股份有限公司</p>
 *
 * @version: 1.0
 * @author: ruansheng
 * @date 5/23/2019
 */
@Data
@Component
@ConfigurationProperties(prefix = "ne.crawler")
public class CrawlerProperties {

    private Schedule schedule = new Schedule();
    private String playlistUrl;
    private String proxyFetchUrl;

    @Data
    public class Schedule {
        private int pageStart = 0;
        private int pageSize = 50;
    }

}
