package com.ruanx.netease.property;

/**
 * @author Ruan Sheng
 * @date 2018/4/22 16:45
 */
public final class RedisConstants {
    /**
     * 音乐id集合
     */
    public static final String MUSIC_ID_SET = "MUSIC_ID_SET";
    /**
     * 歌单id集合
     */
    public static final String PLAYLIST_ID_SET = "PLAYLIST_ID_SET";

    /**
     * 需要排除的音乐ID
     */
    public static final String EXCLUDE_MUSIC_ID_SET = "EXCLUDE_MUSIC_ID_SET";
    /**
     * 需要排除的歌手名字
     */
    public static final String EXCLUDE_SINGER_NAME_SET = "EXCLUDE_SINGER_NAME_SET";
    /**
     * 爬虫忽略的歌单ID集合
     */
    public static final String IGNORED_PLAYLIST_ID_SET = "IGNORED_PLAYLIST_ID_SET";
}
