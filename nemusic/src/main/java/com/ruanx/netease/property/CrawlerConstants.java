package com.ruanx.netease.property;

import com.ruanx.netease.util.HttpUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Ruan Sheng
 * @date 2018/4/19 13:20
 */
public final class CrawlerConstants {
    /**
     * 爬虫超时时间
     */
    public static final int CRAWLER_TIMEOUT = 5;
    /**
     * 获取音乐评论信息用到的POST参数
     */
    private static final String PARAMS = "drTLST8QcWzKBdjSJLBX5IhcjXbJETLmINuj%2Frf" +
            "%2BFXppYKdfMMv3C4vhB0iPkLXrcQrb40TyONlkghHNhwBX4%2BN1S0EJ6qw2kCr6HcVrDnMWjXkZ" +
            "%2Fq4HFFbjU6Doi65fd9GaSLgqQrnEC7lDQ4UzNcZQVCQBeDjatomOcC9W92T6mFjPMZbj9ET7TxS9oFqH";
    private static final String ENC_SEC_KEY =
            "c916e1e1081536aa7f487dd4798191109e19aca6b560969916a0ab73aa6784312b9f61be7763947ea298c221fa7e09c9691a8eb69fd2ff0b6926e5650addf87a1c48e548855cb889a63f28b2a636618246604ea0de446644b5bfb37ecc29748bc308caaa2ee0c909a794eb559f70ba38b2f32f1b4f9021803c7f953d929b9828";
    /**
     * 根据歌单id获取音乐列表
     */
    public static final String URL_MUSIC = "https://music.163.com/playlist?id=";
    /**
     * 获取评论数
     */
    public static final String URL_COMMENT_COUNT = "https://music.163.com/weapi/v1/resource/comments/R_SO_4_";
    /**
     * 获取音乐信息
     */
    public static final String URL_MUSIC_INFO = "https://music.163.com/song?id=";
    /**
     * 默认Cookie,解决IP频繁请求时,无法正常获取音乐信息问题
     */
    private static final String DEFAULT_COOKIE = "vjuids=eea003e7.15b963de102.0.0e91b372; " +
            "usertrack=c+5+hlkF+WB0GCAEGDQDAg==; __utma=187553192.577303493.1500219472.1500219472" +
            ".1500622498.2; __oc_uuid=ba5abbe0-6a3c-11e7-a641-9f3373d2288a; __guid=94650624" +
            ".2332907072380711400.1513241627007.3477; _ngd_tid=%2BImsFa5ciU12FLR6ic1B6YYhcSst%2FPlx; " +
            "P_INFO=irwr43629@163.com|1514969675|0|urs|00&99|null&null&null#fuj&350200#10#0#0|&0||irwr43629" +
            "@163.com; mail_psc_fingerprint=165106321d9180e9c43c019894c1fb5f; __f_=1517917943624; " +
            "vjlast=1492874289.1523355801.11; vinfo_n_f_l_n3=dc2b5518aaa7b916.1.32.1492874289655" +
            ".1522646190322.1523355811012; _ntes_nnid=202c01a3af1aa6488c684a4574626e6c,1524120746823; " +
            "_ntes_nuid=202c01a3af1aa6488c684a4574626e6c; WM_TID=sGyzp3hZczphzKUp7t5AwLJY%2FsYY67Sd; " +
            "JSESSIONID-WYYY=%2BDYMtXa9RWT7%2BDaa1lPlIANc%2F56SAXp%2BHGqXlFlSUz3JqKVQopfjz7pnllYezsH" +
            "%5COkIj0bV9Hk36lXWxqrgFHxdTP5t9rgFWhO2MI5fhZF8irU45ozPTnyoq4y%5CqEvYsz2JeOeoZDsQgG2%2BoRzJnrmC" +
            "%2B8hfgIk5%5CU%5CxeRMwjaO%2F9Gibj%3A1524141064912; _iuqxldmzr_=32; " +
            "MUSIC_U" +
            "=e757881cc7902710dd613ffb85447774f0f411414a0e6c0764376f06e463b6593ed77231748ae08dc3020c94a7f51a22708949fbd8ba3a2d68dbfbd03c808b14401a5d3d8c4b463dbf122d59fa1ed6a2; __remember_me=true; __csrf=8eb5055137843b778892fd3f1c44278c; __utma=94650624.377490417.1492583234.1524127989.1524137525.140; __utmb=94650624.36.10.1524137525; __utmc=94650624; __utmz=94650624.1524137525.140.88.utmcsr=baidu|utmccn=(organic)|utmcmd=organic";
    /**
     * 默认Cookie,解决IP频繁请求时,无法正常获取音乐信息问题
     */
    public static final Map<String, String> COOKIE_MAP;
    /**
     * 获取音乐评论信息用到的POST参数
     */
    public static final Map<String, String> PARAM_MAP;
    /**
     * 获取音乐下载地址
     */
    public static final String URL_MUSIC_DOWNLOAD = "https://music.163.com/weapi/song/enhance/player/url" +
            "?csrf_token=";

    static {
        COOKIE_MAP = HttpUtil.parseCookie(DEFAULT_COOKIE);
        PARAM_MAP = new HashMap<>(2);
        PARAM_MAP.put("params", PARAMS);
        PARAM_MAP.put("encSecKey", ENC_SEC_KEY);
    }
}
