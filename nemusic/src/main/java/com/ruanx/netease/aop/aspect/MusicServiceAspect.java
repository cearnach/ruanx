package com.ruanx.netease.aop.aspect;

import com.google.common.hash.BloomFilter;
import com.ruanx.netease.aop.OnMusicSave;
import com.ruanx.netease.entity.Music;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * <p>标题: </p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2019</p>
 * <p>公司: 智业软件股份有限公司</p>
 *
 * @version: 1.0
 * @author: ruansheng
 * @date 5/15/2019
 */
@Component
@Aspect
@Slf4j
public class MusicServiceAspect {

    private final BloomFilter<String> bloomFilter;

    public MusicServiceAspect(@Qualifier("ALL_MUSIC_ID_BLOOM_FILTER") BloomFilter<String> bloomFilter) {
        this.bloomFilter = bloomFilter;
    }

    @AfterReturning(value = "@annotation(onMusicSave)", returning = "music")
    public void addMusicIdToBloomFilter(OnMusicSave onMusicSave, Music music) {
        bloomFilter.put(music.getId());
        log.info("音乐保存成功,向布隆过滤器添加音乐ID:{}", music.getId());
    }
}
