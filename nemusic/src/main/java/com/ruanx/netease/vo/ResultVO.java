package com.ruanx.netease.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

/**
 * @author 阮胜
 * @date 2018/8/12 18:04
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ResultVO<T> {
    private Integer code;
    private Integer count;
    private T data;
    private String msg;

    public static void main(String[] args) throws IOException {
        Document ids = Jsoup.connect("http://ne.ruanx.fun:35000/music/exclude")
                .data("ids", "123")
                //                .header("Content-Type", "multipart/form-data")
                .post();
        RestTemplate restTemplate = new RestTemplate();
        System.out.println(ids);

    }
}
