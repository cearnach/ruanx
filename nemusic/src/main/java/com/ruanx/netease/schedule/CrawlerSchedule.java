package com.ruanx.netease.schedule;

import com.ruanx.netease.bean.ProxyIpRefresh;
import com.ruanx.netease.crawler.MusicCrawler;
import com.ruanx.netease.crawler.PlaylistCrawler;
import com.ruanx.netease.entity.IgnoreMusicItem;
import com.ruanx.netease.entity.IgnoreMusicItemId;
import com.ruanx.netease.entity.Music;
import com.ruanx.netease.enumeration.IgnoreTypeEnum;
import com.ruanx.netease.exception.StopCrawlerScheduleException;
import com.ruanx.netease.filter.NeFilter;
import com.ruanx.netease.property.CrawlerProperties;
import com.ruanx.netease.service.IgnoreMusicItemService;
import com.ruanx.netease.service.MusicService;
import com.ruanx.netease.util.StringUtils;
import lombok.Cleanup;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.domain.PageRequest;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * @author 阮胜
 * @date 2019/5/12 18:39
 */
@Component
@Slf4j
public class CrawlerSchedule {


    private final MusicService musicService;
    private final IgnoreMusicItemService ignoreMusicItemService;
    private final NeFilter neFilter;
    private final ProxyIpRefresh proxyIpRefresh;
    private final CrawlerProperties crawlerProperties;

    public CrawlerSchedule(MusicService musicService, IgnoreMusicItemService ignoreMusicItemService,
                           NeFilter neFilter, ProxyIpRefresh proxyIpRefresh,
                           CrawlerProperties crawlerProperties) {
        this.musicService = musicService;
        this.ignoreMusicItemService = ignoreMusicItemService;
        this.neFilter = neFilter;
        this.proxyIpRefresh = proxyIpRefresh;
        this.crawlerProperties = crawlerProperties;
    }

    private List<String> getPlaylistTypes() throws IOException {

        ClassPathResource resource = new ClassPathResource("/static/ne/crawler-playlist-type.txt");
        @Cleanup InputStream in = resource.getInputStream();
        @Cleanup InputStreamReader isr = new InputStreamReader(in);
        @Cleanup BufferedReader br = new BufferedReader(isr);
        String line;
        List<String> lines = new ArrayList<>();
        while ((line = br.readLine()) != null) {
            lines.add(line);
        }
        List<String> list = lines.stream().distinct().collect(Collectors.toList());
        Collections.shuffle(list);
        return list;
    }

    /**
     * 歌单爬虫任务
     * 每天下午1点执行：0 0 13 * * ?
     * 周五的凌晨1点执行： 0 0 1 ? * 4
     * 每天晚上十点执行：0 0 22 * * ?
     *
     * @throws IOException
     */
    @Scheduled(cron = "0 0 19 ? * 4")
    public void playlistSchedule() throws IOException {
        this.executePlaylistCrawlerSchedule();
    }

    /**
     * 评论数更新任务
     * 每天凌晨1点执行：0 0 1 * * ?
     * 每周日凌晨1点执行：0 0 1 ? * 1
     * 每天晚上十点执行：0 0 22 * * ?
     */
    @Scheduled(cron = "0 0 19 ? * 1")
    public void commentSchedule() throws IOException, InterruptedException {
        this.updateCommentSchedule();

    }

    /**
     * 更新评论数
     */
    @Async
    public void updateCommentSchedule() throws IOException, InterruptedException {
        log.info("【 评论数更新 - 开始 】 开始执行评论数更新任务");
        int updateCount = 0;
        int page = 0;
        int oftenCount = 0;
        while (true) {
            List<String> musicIdList = musicService.findIdByCommentLess(0, PageRequest.of(page++, 100));
            if (CollectionUtils.isEmpty(musicIdList)) {
                musicIdList = musicService.findIdByUpdateTime(PageRequest.of(page++, 100));
            }
            if (CollectionUtils.isEmpty(musicIdList)) {
                log.info("【 评论数更新 - 结束 】 暂时没有需要更新的音乐信息");
                break;
            }
            for (String musicId : musicIdList) {
                int comment = MusicCrawler.obtainComment(musicId);
                if (comment == -1) {
                    log.info("【 评论数更新 - 更换代理 】 当前评论数为-1,准备更换代理,频繁次数:{}", oftenCount++);
                    proxyIpRefresh.refreshGlobalProxy();
                    if (oftenCount > 10) {
                        log.info("【 评论数更新 - IP频繁 】 超过10次评论数为-1,退出评论数更新任务,更新数量:{}", updateCount);
                        proxyIpRefresh.reset();
                        return;
                    }
                    continue;
                }
                musicService.updateComment(musicId, comment);
                updateCount++;
                log.info("【 评论数更新 - 成功 】 音乐ID : {} , 评论数: {} , 已更新数量 : {}", musicId, comment, updateCount);
            }
        }
        proxyIpRefresh.reset();
        long allMusicCount = musicService.count();
        log.info("【 评论数更新 - 结束 】 更新数量:{} , 总音乐数量：{}", updateCount, allMusicCount);

    }

    /**
     * 根据歌单获取音乐数据
     *
     * @throws IOException
     */
    @Async
    public void executePlaylistCrawlerSchedule() throws IOException {
        int pageStart = crawlerProperties.getSchedule().getPageStart();
        int pageSize = crawlerProperties.getSchedule().getPageSize();
        if (pageStart >= 50) {
            pageStart = 0;
        }
        AtomicInteger count = new AtomicInteger();
        List<String> types = this.getPlaylistTypes();
        Collections.shuffle(types);
        log.info("【 歌单爬虫任务 - 开始 】 开始根据歌单执行爬虫,歌单类型列表:{}", StringUtils.collectionToCommaDelimitedString(types));
        int nullTitleCount = 0;
        try {
            for (int i = pageStart; i < pageSize; i++) {
                for (String playlistType : types) {
                    int offset = i * 35;
                    log.info("【 歌单爬虫任务 - 进行中 进度:{}/{}】- 歌单类型:{}", i + 1, pageSize, playlistType);
                    String html = null;
                    try {
                        html = PlaylistCrawler.obtainPlaylistHtmlByName(playlistType, offset);
                    } catch (IOException e) {
                        log.error("根据名称获取歌单页面HTML失败 , 准备更换代理");
                        proxyIpRefresh.refreshGlobalProxy();
                    }
                    Set<String> playlistIdSet = PlaylistCrawler.obtainPlaylistIdByHtml(html);
                    List<String> notExistsPlaylistIdSet = neFilter.notExistsByPlaylistIdBatch(playlistIdSet);
                    log.info("【 歌单爬虫任务 - 进行中 进度:{}/{}】- 共{}个歌单,过滤{}个歌单,需要获取歌单数量:{}",
                            i + 1, pageSize, playlistIdSet.size(),
                            playlistIdSet.size() - notExistsPlaylistIdSet.size(),
                            notExistsPlaylistIdSet.size());
                    for (String playlistId : notExistsPlaylistIdSet) {
                        if (neFilter.existsByPlaylistId(playlistId)) {
                            log.info("【 歌单爬虫任务 - 进行中 - {} - {} 】 歌单ID已经爬取过 , 自动跳过", playlistType, playlistId);
                            continue;
                        }
                        try {
                            List<String> musicIdList =
                                    PlaylistCrawler.obtainMusicIdListByPlaylistId(playlistId);
                            List<String> notExistsIdList = neFilter.notExistsByMusicIdBatch(musicIdList);
                            log.info("【 歌单爬虫任务 - 进行中 - {} 进度:{}/{}】 过滤掉{}/{}个已存在ID , 准备抓取{}条音乐记录",
                                    playlistType, i + 1, pageSize,
                                    musicIdList.size() - notExistsIdList.size(),
                                    musicIdList.size(), notExistsIdList.size());

                            Set<Music> musicSet = MusicCrawler.obtainMusicInfoByMusicIdList(notExistsIdList);
                            List<Music> nullTitleMusicList = musicSet.stream()
                                    .filter(music -> StringUtils.isEmpty(music.getTitle()))
                                    .collect(Collectors.toList());

                            if (checkProxy(nullTitleMusicList)) {
                                List<String> tmpMusicIdList = nullTitleMusicList.stream()
                                        .map(Music::getId)
                                        .collect(Collectors.toList());
                                musicSet = musicSet.stream()
                                        .filter(music -> !StringUtils.isEmpty(music.getTitle()))
                                        .collect(Collectors.toSet());
                                System.gc();
                                Set<Music> tmpMusicSet =
                                        MusicCrawler.obtainMusicInfoByMusicIdList(tmpMusicIdList);
                                musicSet.addAll(tmpMusicSet);
                            } else {
                                log.info("【 歌单爬虫任务 - 终止 】代理池耗尽,终止任务,增加音乐数:{}", count.get());
                                return;
                            }

                            // 计算空标题的音乐数量
                            long filteredNullTitleCount = musicSet.stream()
                                    .filter(music -> StringUtils.isEmpty(music.getTitle()))
                                    .count();

                            log.info("【 歌单爬虫任务 - 进行中 - {} 进度:{}/{} 】 过滤掉{}/{}条标题为空的音乐记录",
                                    playlistType, i + 1, pageSize, filteredNullTitleCount, musicSet.size());

                            // 对结果集进行排序,将空标题的结果放到最后面
                            List<Music> sortedMusicList = musicSet.stream()
                                    .peek(music -> {
                                        if (StringUtils.isEmpty(music.getTitle())) {
                                            music.setTitle("");
                                        }
                                    }).sorted(Comparator.comparingInt(m -> -m.getTitle().length()))
                                    .collect(Collectors.toList());

                            for (Music music : sortedMusicList) {
                                if (nullTitleCount > 50) {
                                    proxyIpRefresh.reset();
                                    throw new StopCrawlerScheduleException();
                                }
                                if (StringUtils.isEmpty(music.getTitle())) {
                                    log.info("音乐标题为空 , 自动跳过");
                                    nullTitleCount++;
                                    proxyIpRefresh.refreshGlobalProxy();
                                    break;
                                }
                                // 当音乐标题不为空的时候,清空标记
                                nullTitleCount = 0;
                                try {
                                    if (neFilter.existsByMusicId(music.getId())) {
                                        log.info("音乐已经存在,自动跳过");
                                        continue;
                                    }
                                    musicService.save(music);
                                    count.incrementAndGet();
                                } catch (Exception e) {
                                    log.error("【 歌单爬虫任务 - 异常 - {} 】保存音乐时发生错误 , 音乐详情:{}", playlistType, music);
                                    e.printStackTrace();
                                }
                            }
                            ignoreMusicItemService.save(new IgnoreMusicItem(new IgnoreMusicItemId(playlistId,
                                    IgnoreTypeEnum.PLAYLIST_ID)));
                        } catch (StopCrawlerScheduleException e) {
                            throw e;
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        } catch (StopCrawlerScheduleException | IOException | InterruptedException e) {
            log.error(e.getMessage());
        }

        proxyIpRefresh.reset();
        log.info("【 歌单爬虫任务 - 完成 】根据歌单执行爬虫完成,增加音乐数:{}", count.get());
    }

    /**
     * 检查集合中是否有标题为空的音乐数据,如果有则设置新的代理
     *
     * @param nullTitleMusicList
     * @return
     * @throws Exception
     */
    private boolean checkProxy(List<Music> nullTitleMusicList) throws Exception {
        log.info("【 歌单爬虫任务 - 进行中 】 开始检测集合中是否有空标题记录");
        int maxCheck = 100;
        if (!CollectionUtils.isEmpty(nullTitleMusicList)) {
            log.info("【 歌单爬虫任务 - 进行中 】 发现歌单中有{}条空标题记录,准备更换代理", nullTitleMusicList.size());
            proxyIpRefresh.refreshGlobalProxy();
            for (int k = 0; k < nullTitleMusicList.size(); k++) {
                Music music = nullTitleMusicList.get(k);
                if (StringUtils.isEmpty(music.getId())) {
                    log.error("【 歌单爬虫任务 - 错误 】 更新代理IP时,发现音乐ID为空,无法检测代理有效性,退出任务");
                    return false;
                }
                Music newMusic = MusicCrawler.obtainMusicInfoById(music.getId());
                log.error("【 歌单爬虫任务 - 代理验证 】 代理后获取的音乐详情:{}", newMusic);
                if (StringUtils.isEmpty(newMusic.getTitle())) {
                    k--;
                    if (++maxCheck > 100) {
                        log.error("【 歌单爬虫任务 - 错误 】 检查代理有效性超过100次,自动退出任务");
                        return false;
                    }
                    proxyIpRefresh.refreshGlobalProxy();
                } else {
                    break;
                }
            }
        }
        return true;
    }

}
