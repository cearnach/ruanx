package com.ruanx.netease.multithreading;

import lombok.extern.slf4j.Slf4j;

import java.util.Collection;
import java.util.concurrent.CountDownLatch;

/**
 * 多线程爬虫基类,如果有其他的自定义需求,可以继承该类进行扩展
 * @author Ruan Sheng
 * @date 2018/4/23 16:35
 */
@Slf4j
public abstract class BaseMultithreadingCrawler<E> implements Runnable {
    protected String id;
    final Collection<E> crawlerCollection;
    CountDownLatch latch;

    BaseMultithreadingCrawler(String id, Collection<E> crawlerCollection, CountDownLatch latch) {
        this.id = id;
        this.crawlerCollection = crawlerCollection;
        this.latch = latch;
    }

}
