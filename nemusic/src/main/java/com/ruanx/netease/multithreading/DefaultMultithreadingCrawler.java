package com.ruanx.netease.multithreading;

import lombok.extern.slf4j.Slf4j;

import java.util.Collection;
import java.util.concurrent.CountDownLatch;

/**
 * 多线程爬虫的默认实现
 *
 * @author Ruan Sheng
 * @date 2018/4/23 17:18
 */
@Slf4j
public class DefaultMultithreadingCrawler<E> extends BaseMultithreadingCrawler<E> {
    private CollectionCrawler<E> collectionCrawler;
    private EntityCrawler<E> entityCrawler;

    public interface CollectionCrawler<T> {
        /**
         * 集合类爬虫执行块
         *
         * @return 爬虫结果集合
         * @throws Exception 中断异常
         */
        Collection<T> doCrawler() throws Exception;
    }

    public interface EntityCrawler<T> {
        /**
         * 实体类爬虫执行块
         *
         * @return 爬虫结果实体
         * @throws Exception 中断异常
         */
        T doCrawler() throws Exception;
    }

    public DefaultMultithreadingCrawler(String id, Collection<E> crawlerCollection, CountDownLatch latch,
                                        CollectionCrawler<E> collectionCrawler) {
        super(id, crawlerCollection, latch);
        this.collectionCrawler = collectionCrawler;
    }

    public DefaultMultithreadingCrawler(String id, Collection<E> crawlerCollection, CountDownLatch latch,
                                        EntityCrawler<E> entityCrawler) {
        super(id, crawlerCollection, latch);
        this.entityCrawler = entityCrawler;
    }

    @Override
    public void run() {
        try {
            if (collectionCrawler == null) {
                E e = entityCrawler.doCrawler();
                crawlerCollection.add(e);
            } else {
                Collection<E> collection = collectionCrawler.doCrawler();
                crawlerCollection.addAll(collection);
            }
        } catch (Exception e) {
            log.error(e.getMessage());
        } finally {
            latch.countDown();
        }
    }
}
