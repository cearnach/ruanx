package com.ruanx.netease.entity;

import com.ruanx.netease.enumeration.IgnoreTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
public class IgnoreMusicItemId implements Serializable {

    private String item;

    private Integer type;

    public IgnoreMusicItemId(String item, IgnoreTypeEnum ignoreTypeEnum) {
        this.item = item;
        this.type = ignoreTypeEnum.getCode();
    }
}
