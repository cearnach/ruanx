package com.ruanx.netease.entity;


import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;
import java.util.Objects;

/**
 * @author Ruan
 */

@Data
@Entity
@Table(name = "ne_music")
public class Music {
    @Id
    private String id;

    private String title;

    private int comment;

    private String singer;

    private String duration;

    @Column(columnDefinition = "INT AUTO_INCREMENT,KEY(`flag`)")
    private Integer flag;

    @Temporal(TemporalType.TIMESTAMP)
    @UpdateTimestamp
    private Date updateTime;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(updatable = false)
    @CreationTimestamp
    private Date createTime;

    public Music() {
    }

    public Music(String id, String title, int comment, String singer, String duration) {
        this.id = id;
        this.title = title;
        this.comment = comment;
        this.singer = singer;
        this.duration = duration;
    }

    public Music(String id, String title, int comment, String singer) {
        this.id = id;
        this.title = title;
        this.comment = comment;
        this.singer = singer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Music music = (Music) o;
        return Objects.equals(id, music.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
