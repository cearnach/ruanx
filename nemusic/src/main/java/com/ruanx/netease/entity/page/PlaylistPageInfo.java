package com.ruanx.netease.entity.page;

import com.ruanx.netease.entity.Playlist;
import lombok.Data;

import java.util.List;

/**
 * @author Ruan
 * @date 2018/4/19 13:14
 */
@Data
public final class PlaylistPageInfo {
    private Playlist playlist;
    private List<String> musicIdList;
    private List<String> relevancePlaylistIdList;
}
