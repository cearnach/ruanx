package com.ruanx.netease.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * @author Ruan
 */
@Data
@Entity
@Table(name = "ne_playlist")
public class Playlist implements Serializable {
    /**
     * 歌单id
     */
    @Id
    private String id;

    /**
     * 歌单名称
     */
    private String title;

    /**
     * 歌单音乐数量
     */
    private int musicCount;

    /**
     * 歌单播放次数
     */
    private int playCount;

    /**
     * 收藏数
     */
    private int favoriteCount;

    /**
     * 歌单评论数
     */
    private int commentCount;

    /**
     * 更新时间
     */
    @Column(columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP")
    private Date updateTime;

    public Playlist() {
    }

    public Playlist(String title, String id, int musicCount, int playCount, int favoriteCount,
                    int commentCount) {
        this.title = title;
        this.id = id;
        this.musicCount = musicCount;
        this.playCount = playCount;
        this.favoriteCount = favoriteCount;
        this.commentCount = commentCount;
    }

    public Playlist(String title, String id, int musicCount, int playCount, int favoriteCount,
                    int commentCount, Date updateTime) {

        this.title = title;
        this.id = id;
        this.musicCount = musicCount;
        this.playCount = playCount;
        this.favoriteCount = favoriteCount;
        this.commentCount = commentCount;
        this.updateTime = updateTime;
    }
}
