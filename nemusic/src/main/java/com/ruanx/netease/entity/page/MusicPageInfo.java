package com.ruanx.netease.entity.page;

import com.ruanx.netease.entity.Music;
import lombok.Data;

import java.util.List;

/**
 * @author Ruan Sheng
 * @date 2018/4/20 11:23
 */
@Data
public final class MusicPageInfo {
    private Music music;
    private List<String> relevancePlaylistIdList;
    private List<String> similarMusicIdList;

}
