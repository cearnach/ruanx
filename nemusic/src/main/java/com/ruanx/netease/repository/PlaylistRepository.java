package com.ruanx.netease.repository;

import com.ruanx.netease.entity.Playlist;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Ruan Sheng
 * @date 2018/4/23 19:25
 */
public interface PlaylistRepository extends JpaRepository<Playlist, String> {

}
