package com.ruanx.netease.repository;

import com.ruanx.netease.entity.Music;
import com.ruanx.netease.repository.base.BaseRepository;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author Ruan Sheng
 * @date 2018/4/19 22:48
 */
public interface MusicRepository extends BaseRepository<Music, String> {

    /**
     * 获取所有id集合
     *
     * @return
     */
    @Query(value = "SELECT id FROM ne_music ORDER BY flag", nativeQuery = true)
    List<String> findAllId();

    @Query(value = "SELECT id FROM ne_music ORDER BY flag", nativeQuery = true)
    List<String> findAllId(Pageable pageable);

    @Query(value = "SELECT * FROM ne_music WHERE duration = '00:00' ORDER BY COMMENT DESC LIMIT ?",
            nativeQuery = true)
    List<Music> findAllByNullDuration(int limit);

    @Query(value = "SELECT id FROM ne_music  WHERE flag > :flag", nativeQuery = true)
    List<String> findIdByFlagGreaterThan(Integer flag, Pageable pageable);

    @Query(value = "SELECT id FROM ne_music m WHERE m.comment < :comment", nativeQuery = true)
    List<String> findIdByCommentLess(int comment, Pageable pageable);

    @Query(value = "UPDATE ne_music m SET m.comment = :comment WHERE m.id = :musicId", nativeQuery = true)
    @Modifying
    int updateComment(String musicId, int comment);

    @Query(value = "SELECT id FROM ne_music " +
            "WHERE DATE(update_time) <= DATE_SUB(CURDATE(), INTERVAL 30 DAY) " +
            "AND COMMENT > 100 " +
            "ORDER BY update_time", nativeQuery = true)
    List<String> findIdByUpdateTime(Pageable pageable);

    @Query(value = "SELECT * " +
            "FROM ne_music m " +
            "  WHERE m.title REGEXP \"[u0391-uFFE5]\" = 1 " +
            "  AND m.singer REGEXP \"[u0391-uFFE5]\" = 1 " +
            "  AND m.singer NOT IN ( " +
            "    SELECT i.item " +
            "    FROM ne_ignore_music_item i " +
            "    WHERE i.type = 3 " +
            ") " +
            "  AND m.id NOT IN ( " +
            "    SELECT i.item " +
            "    FROM ne_ignore_music_item i " +
            "    WHERE i.type = 1 " +
            ") " +
            " ORDER BY m.comment DESC " +
            " LIMIT :size ", nativeQuery = true)
    List<Music> findTop(int size);
}
