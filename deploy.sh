#!/bin/sh
serviceName=ruanx
git pull
mvn clean package -Dmaven.test.skip=true
docker stop $serviceName
docker rm $serviceName
docker rmi $serviceName
docker build . -t $serviceName
docker run -d -v /etc/timezone:/etc/timezone -v /etc/localtime:/etc/localtime --restart=always --name $serviceName -p 35000:35000 $serviceName
docker logs $serviceName -t -f --tail 100

