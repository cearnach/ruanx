package com.ruanx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * <p>标题: </p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2020</p>
 * <p>公司: 智业软件股份有限公司</p>
 *
 * @version: 1.0
 * @author: ruansheng
 * @date: 2020-02-26
 */
@SpringBootApplication
@EnableAsync
@EnableScheduling
public class RuanxApplication {
    public static void main(String[] args) {
        SpringApplication.run(RuanxApplication.class, args);
    }
}
