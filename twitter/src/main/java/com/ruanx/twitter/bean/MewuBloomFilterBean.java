package com.ruanx.twitter.bean;


import com.ruanx.twitter.enumeration.IgnoreTypeEnum;
import com.ruanx.twitter.service.IgnoreItemService;
import com.google.common.hash.BloomFilter;
import com.google.common.hash.Funnels;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.nio.charset.Charset;
import java.util.List;

/**
 * <p>标题: </p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2019</p>
 * <p>公司: 智业软件股份有限公司</p>
 *
 * @version: 1.0
 * @author: ruansheng
 * @date 5/15/2019
 */
@Configuration
@Slf4j
public class MewuBloomFilterBean {

    private final IgnoreItemService ignoreItemService;

    public MewuBloomFilterBean(IgnoreItemService ignoreItemService) {
        this.ignoreItemService = ignoreItemService;
    }

    @Bean("mewu-bloom-filter")
    public BloomFilter<String> excludeMusicIdFilter() {
        BloomFilter<String> bloomFilter = BloomFilter
                .create(Funnels.stringFunnel(Charset.defaultCharset()), 1024 * 10, 0.0001);
        List<String> ids = ignoreItemService.findAllByType(IgnoreTypeEnum.TWITTER_ACCOUNT);
        ids.forEach(bloomFilter::put);
        return bloomFilter;
    }
}
