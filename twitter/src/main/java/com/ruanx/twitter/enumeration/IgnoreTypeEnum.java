package com.ruanx.twitter.enumeration;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * <p>标题: </p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2019</p>
 * <p>公司: 智业软件股份有限公司</p>
 *
 * @version: 1.0
 * @author: ruansheng
 * @date 5/13/2019
 */
@Getter
@AllArgsConstructor
public enum IgnoreTypeEnum {
    /**
     * CODE = 代号 , NAME = 状态名称
     */
    TWITTER_ACCOUNT(1, "推特账户"),
    TWITTER_USER_ID(2, "推特用户ID");

    private Integer code;
    private String name;

    public static IgnoreTypeEnum findByCode(Integer code) {
        for (IgnoreTypeEnum sellerStatusEnum : IgnoreTypeEnum.values()) {
            if (sellerStatusEnum.getCode().equals(code)) {
                return sellerStatusEnum;
            }
        }
        return null;
    }
}
