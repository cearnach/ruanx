package com.ruanx.twitter.exception;


/**
 * @author 阮胜
 * @date 2018/7/2 19:39
 */
public class TargetEntityNotFound extends BaseException {
    private static final String TARGET_ENTITY_NOT_FOUND = "找不到目标实体";
    private static final long serialVersionUID = 3045474131371244729L;

    public TargetEntityNotFound() {
        super(TARGET_ENTITY_NOT_FOUND);
    }

    public TargetEntityNotFound(String message) {
        super(message);
    }
}
