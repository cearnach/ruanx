package com.ruanx.twitter.service.base;


import com.ruanx.twitter.bean.PageBean;
import com.ruanx.twitter.exception.TargetEntityNotFound;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

/**
 * @author 阮胜
 * @date 2018/7/2 19:45
 */
public interface BaseService<T, ID extends Serializable> {
    /**
     * 根据id查询实体
     *
     * @param id 实体id
     * @return 返回实体类
     * @throws TargetEntityNotFound 查询不到实体异常
     */
    T findById(ID id) throws TargetEntityNotFound;

    Optional<T> findByIdOptional(ID id);

    /**
     * 保存实体
     *
     * @param t
     * @return
     */
    T save(T t);

    /**
     * 删除实体
     *
     * @param id
     * @throws TargetEntityNotFound
     */
    void delete(ID id) throws TargetEntityNotFound, Exception;

    /**
     * 根据ID判断实体是否存在
     * @param id
     * @return
     */
    boolean existsById(ID id);

    /**
     * 不分页查询所有
     *
     * @return
     */
    List<T> findAll();

    /**
     * 分页查询所有
     *
     * @param pageable
     * @return
     */
    Page<T> findAll(Pageable pageable);

    /**
     * 分页查询所有
     *
     * @param pageBean
     * @return
     */
    Page<T> findAll(PageBean pageBean);

    /**
     * 分页查询所有
     *
     * @param pageBean
     * @param direction
     * @param properties
     * @return
     */
    Page<T> findAll(PageBean pageBean, Sort.Direction direction, String... properties);

    /**
     * 根据id数组批量删除
     *
     * @param idArr 要删除的id数组
     * @return 返回删除失败的id集合
     */
    List<ID> deleteIn(ID[] idArr);

    /**
     * 取实体数量
     *
     * @return
     */
    long count();


    /**
     * 根据id集合批量查询所有
     *
     * @param ids
     * @return
     */
    List<T> findAllById(List<ID> ids);

    /**
     * 根据参数模型保存对象,该方法会将传入的实体类参数拷贝到对应的数据库模型实体类,默认不会拷贝null字段,empty字段会拷贝
     *
     * @param target 要保存的对象参数模型
     * @return
     * @throws Exception
     */
    T saveParam(Object target) throws Exception;

}
