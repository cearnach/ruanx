package com.ruanx.twitter.service;


import com.ruanx.twitter.entity.IgnoreTwitterItem;
import com.ruanx.twitter.entity.IgnoreTwitterItemId;
import com.ruanx.twitter.enumeration.IgnoreTypeEnum;
import com.ruanx.twitter.service.base.BaseService;

import java.util.List;

/**
 * <p>标题: </p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2019</p>
 * <p>公司: 智业软件股份有限公司</p>
 *
 * @version: 1.0
 * @author: ruansheng
 * @date 5/13/2019
 */
public interface IgnoreItemService extends BaseService<IgnoreTwitterItem, IgnoreTwitterItemId> {
    List<String> findAllByType(IgnoreTypeEnum musicId);
}
