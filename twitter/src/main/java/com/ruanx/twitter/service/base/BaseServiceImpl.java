package com.ruanx.twitter.service.base;


import com.ruanx.twitter.bean.PageBean;
import com.ruanx.twitter.exception.TargetEntityNotFound;
import com.ruanx.twitter.repository.base.BaseRepository;
import com.ruanx.twitter.utils.BeanUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author 阮胜
 * @date 2018/7/2 19:35
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
@Slf4j
public class BaseServiceImpl<Entity, ID extends Serializable, Repository extends BaseRepository<Entity, ID>>
        implements BaseService<Entity, ID> {

    private final Class<Entity> entityClass;

    @SuppressWarnings("unchecked")
    public BaseServiceImpl() {
        this.entityClass = (Class<Entity>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];

    }

    @Autowired
    protected Repository repository;

    @Override
    public Entity findById(ID id) throws TargetEntityNotFound {
        return repository.findById(id).orElseThrow(TargetEntityNotFound::new);
    }

    @Override
    public Optional<Entity> findByIdOptional(ID id) {
        return repository.findById(id);
    }

    @Override
    public Entity save(Entity entity) {
        return repository.save(entity);
    }

    @Override
    public void delete(ID id) throws Exception {
        findById(id);
        repository.deleteById(id);
    }

    @Override
    public boolean existsById(ID id) {
        return repository.existsById(id);
    }

    @Override
    public List<Entity> findAll() {
        return repository.findAll();
    }

    @Override
    public Page<Entity> findAll(Pageable pageable) {
        return repository.findAll(pageable);
    }

    @Override
    public Page<Entity> findAll(PageBean pageBean) {
        return repository.findAll(pageBean.toPageable());
    }

    @Override
    public Page<Entity> findAll(PageBean pageBean, Sort.Direction direction, String... properties) {
        return repository.findAll(pageBean.toPageable(Sort.by(direction, properties)));
    }


    @Override
    public List<ID> deleteIn(ID[] idArr) {
        List<ID> failedIdList = null;
        for (ID id : idArr) {
            try {
                delete(id);
            } catch (Exception e) {
                log.error(e.getMessage());
                if (failedIdList == null) {
                    failedIdList = new ArrayList<>();
                }
                failedIdList.add(id);
            }
        }
        return failedIdList;
    }

    @Override
    public long count() {
        return repository.count();
    }

    @Override
    public List<Entity> findAllById(List<ID> ids) {
        return repository.findAllById(ids);
    }

    @Override
    public Entity saveParam(Object target) throws Exception {
        Entity entity = this.entityClass.newInstance();
        BeanUtils.copyPropertiesIgnoreNull(target, entity);
        return repository.save(entity);
    }

}
