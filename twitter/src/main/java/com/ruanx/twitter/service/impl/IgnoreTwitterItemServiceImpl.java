package com.ruanx.twitter.service.impl;


import com.ruanx.twitter.entity.IgnoreTwitterItem;
import com.ruanx.twitter.entity.IgnoreTwitterItemId;
import com.ruanx.twitter.enumeration.IgnoreTypeEnum;
import com.ruanx.twitter.repository.IgnoreTwitterItemRepository;
import com.ruanx.twitter.service.IgnoreItemService;
import com.ruanx.twitter.service.base.BaseServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>标题: </p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2019</p>
 * <p>公司: 智业软件股份有限公司</p>
 *
 * @version: 1.0
 * @author: ruansheng
 * @date 5/13/2019
 */
@Service
public class IgnoreTwitterItemServiceImpl extends BaseServiceImpl<IgnoreTwitterItem, IgnoreTwitterItemId, IgnoreTwitterItemRepository>
        implements IgnoreItemService {

    @Override
    public List<String> findAllByType(IgnoreTypeEnum musicId) {
        return repository.findAllByType(musicId.getCode());
    }

    @Override
    public IgnoreTwitterItem save(IgnoreTwitterItem ignoreTwitterItem) {
        return super.save(ignoreTwitterItem);
    }
}
