package com.ruanx.twitter.repository.base;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.io.Serializable;

/**
 * @author 阮胜
 * @date 2018/7/2 16:46
 */
@NoRepositoryBean
public interface BaseRepository<Entity, ID extends Serializable> extends JpaRepository<Entity, ID> {
}
