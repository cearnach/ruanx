package com.ruanx.twitter.repository;


import com.ruanx.twitter.entity.IgnoreTwitterItem;
import com.ruanx.twitter.entity.IgnoreTwitterItemId;
import com.ruanx.twitter.repository.base.BaseRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * <p>标题: </p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2019</p>
 * <p>公司: 智业软件股份有限公司</p>
 *
 * @version: 1.0
 * @author: ruansheng
 * @date 5/13/2019
 */
public interface IgnoreTwitterItemRepository extends BaseRepository<IgnoreTwitterItem, IgnoreTwitterItemId> {

    @Query(value = "select item from twitter_ignore_item as t where t.type = :type", nativeQuery = true)
    List<String> findAllByType(Integer type);
}
