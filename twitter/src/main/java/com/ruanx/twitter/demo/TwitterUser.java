package com.ruanx.twitter.demo;

import java.util.List;

/**
 * <p>标题: </p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2019</p>
 * <p>公司: 智业软件股份有限公司</p>
 *
 * @version: 1.0
 * @author: ruansheng
 * @date: 2019-11-21
 */
public class TwitterUser {

    /**
     * advertiser_account_service_levels : []
     * advertiser_account_type : none
     * blocked_by : false
     * blocking : false
     * business_profile_state : none
     * can_media_tag : true
     * contributors_enabled : false
     * created_at : Mon Oct 14 17:04:24 +0000 2019
     * default_profile : true
     * default_profile_image : false
     * description : 私信我💦
     * entities : {"description":{"urls":[]}}
     * fast_followers_count : 0
     * favourites_count : 5
     * follow_request_sent : false
     * followed_by : false
     * followers_count : 4
     * following : false
     * friends_count : 94
     * geo_enabled : false
     * has_custom_timelines : false
     * has_extended_profile : true
     * id : 1183790632407461889
     * id_str : 1183790632407461889
     * is_translation_enabled : false
     * is_translator : false
     * listed_count : 0
     * live_following : false
     * location : Yuen Long District, Hong Kong
     * media_count : 0
     * muting : false
     * name : 為你瘋狂
     * normal_followers_count : 4
     * notifications : false
     * pinned_tweet_ids : []
     * pinned_tweet_ids_str : []
     * profile_background_color : F5F8FA
     * profile_background_tile : false
     * profile_banner_url : https://pbs.twimg.com/profile_banners/1183790632407461889/1573494917
     * profile_image_url : http://pbs.twimg.com/profile_images/1187187795376267264/VMkuejij_normal.jpg
     * profile_image_url_https : https://pbs.twimg.com/profile_images/1187187795376267264/VMkuejij_normal.jpg
     * profile_interstitial_type :
     * profile_link_color : 1DA1F2
     * profile_sidebar_border_color : C0DEED
     * profile_sidebar_fill_color : DDEEF6
     * profile_text_color : 333333
     * profile_use_background_image : true
     * protected : false
     * require_some_consent : false
     * screen_name : wilson06121
     * statuses_count : 88
     * translator_type : none
     * verified : false
     * want_retweets : false
     */

    private String advertiser_account_type;
    private boolean blocked_by;
    private boolean blocking;
    private String business_profile_state;
    private boolean can_media_tag;
    private boolean contributors_enabled;
    private String created_at;
    private boolean default_profile;
    private boolean default_profile_image;
    private String description;
    private EntitiesBean entities;
    private int fast_followers_count;
    private int favourites_count;
    private boolean follow_request_sent;
    private boolean followed_by;
    private int followers_count;
    private boolean following;
    private int friends_count;
    private boolean geo_enabled;
    private boolean has_custom_timelines;
    private boolean has_extended_profile;
    private long id;
    private String id_str;
    private boolean is_translation_enabled;
    private boolean is_translator;
    private int listed_count;
    private boolean live_following;
    private String location;
    private int media_count;
    private boolean muting;
    private String name;
    private int normal_followers_count;
    private boolean notifications;
    private String profile_background_color;
    private boolean profile_background_tile;
    private String profile_banner_url;
    private String profile_image_url;
    private String profile_image_url_https;
    private String profile_interstitial_type;
    private String profile_link_color;
    private String profile_sidebar_border_color;
    private String profile_sidebar_fill_color;
    private String profile_text_color;
    private boolean profile_use_background_image;
    private boolean protectedX;
    private boolean require_some_consent;
    private String screen_name;
    private int statuses_count;
    private String translator_type;
    private boolean verified;
    private boolean want_retweets;
    private List<?> advertiser_account_service_levels;
    private List<?> pinned_tweet_ids;
    private List<?> pinned_tweet_ids_str;

    public String getAdvertiser_account_type() {
        return advertiser_account_type;
    }

    public void setAdvertiser_account_type(String advertiser_account_type) {
        this.advertiser_account_type = advertiser_account_type;
    }

    public boolean isBlocked_by() {
        return blocked_by;
    }

    public void setBlocked_by(boolean blocked_by) {
        this.blocked_by = blocked_by;
    }

    public boolean isBlocking() {
        return blocking;
    }

    public void setBlocking(boolean blocking) {
        this.blocking = blocking;
    }

    public String getBusiness_profile_state() {
        return business_profile_state;
    }

    public void setBusiness_profile_state(String business_profile_state) {
        this.business_profile_state = business_profile_state;
    }

    public boolean isCan_media_tag() {
        return can_media_tag;
    }

    public void setCan_media_tag(boolean can_media_tag) {
        this.can_media_tag = can_media_tag;
    }

    public boolean isContributors_enabled() {
        return contributors_enabled;
    }

    public void setContributors_enabled(boolean contributors_enabled) {
        this.contributors_enabled = contributors_enabled;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public boolean isDefault_profile() {
        return default_profile;
    }

    public void setDefault_profile(boolean default_profile) {
        this.default_profile = default_profile;
    }

    public boolean isDefault_profile_image() {
        return default_profile_image;
    }

    public void setDefault_profile_image(boolean default_profile_image) {
        this.default_profile_image = default_profile_image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public EntitiesBean getEntities() {
        return entities;
    }

    public void setEntities(EntitiesBean entities) {
        this.entities = entities;
    }

    public int getFast_followers_count() {
        return fast_followers_count;
    }

    public void setFast_followers_count(int fast_followers_count) {
        this.fast_followers_count = fast_followers_count;
    }

    public int getFavourites_count() {
        return favourites_count;
    }

    public void setFavourites_count(int favourites_count) {
        this.favourites_count = favourites_count;
    }

    public boolean isFollow_request_sent() {
        return follow_request_sent;
    }

    public void setFollow_request_sent(boolean follow_request_sent) {
        this.follow_request_sent = follow_request_sent;
    }

    public boolean isFollowed_by() {
        return followed_by;
    }

    public void setFollowed_by(boolean followed_by) {
        this.followed_by = followed_by;
    }

    public int getFollowers_count() {
        return followers_count;
    }

    public void setFollowers_count(int followers_count) {
        this.followers_count = followers_count;
    }

    public boolean isFollowing() {
        return following;
    }

    public void setFollowing(boolean following) {
        this.following = following;
    }

    public int getFriends_count() {
        return friends_count;
    }

    public void setFriends_count(int friends_count) {
        this.friends_count = friends_count;
    }

    public boolean isGeo_enabled() {
        return geo_enabled;
    }

    public void setGeo_enabled(boolean geo_enabled) {
        this.geo_enabled = geo_enabled;
    }

    public boolean isHas_custom_timelines() {
        return has_custom_timelines;
    }

    public void setHas_custom_timelines(boolean has_custom_timelines) {
        this.has_custom_timelines = has_custom_timelines;
    }

    public boolean isHas_extended_profile() {
        return has_extended_profile;
    }

    public void setHas_extended_profile(boolean has_extended_profile) {
        this.has_extended_profile = has_extended_profile;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getId_str() {
        return id_str;
    }

    public void setId_str(String id_str) {
        this.id_str = id_str;
    }

    public boolean isIs_translation_enabled() {
        return is_translation_enabled;
    }

    public void setIs_translation_enabled(boolean is_translation_enabled) {
        this.is_translation_enabled = is_translation_enabled;
    }

    public boolean isIs_translator() {
        return is_translator;
    }

    public void setIs_translator(boolean is_translator) {
        this.is_translator = is_translator;
    }

    public int getListed_count() {
        return listed_count;
    }

    public void setListed_count(int listed_count) {
        this.listed_count = listed_count;
    }

    public boolean isLive_following() {
        return live_following;
    }

    public void setLive_following(boolean live_following) {
        this.live_following = live_following;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getMedia_count() {
        return media_count;
    }

    public void setMedia_count(int media_count) {
        this.media_count = media_count;
    }

    public boolean isMuting() {
        return muting;
    }

    public void setMuting(boolean muting) {
        this.muting = muting;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNormal_followers_count() {
        return normal_followers_count;
    }

    public void setNormal_followers_count(int normal_followers_count) {
        this.normal_followers_count = normal_followers_count;
    }

    public boolean isNotifications() {
        return notifications;
    }

    public void setNotifications(boolean notifications) {
        this.notifications = notifications;
    }

    public String getProfile_background_color() {
        return profile_background_color;
    }

    public void setProfile_background_color(String profile_background_color) {
        this.profile_background_color = profile_background_color;
    }

    public boolean isProfile_background_tile() {
        return profile_background_tile;
    }

    public void setProfile_background_tile(boolean profile_background_tile) {
        this.profile_background_tile = profile_background_tile;
    }

    public String getProfile_banner_url() {
        return profile_banner_url;
    }

    public void setProfile_banner_url(String profile_banner_url) {
        this.profile_banner_url = profile_banner_url;
    }

    public String getProfile_image_url() {
        return profile_image_url;
    }

    public void setProfile_image_url(String profile_image_url) {
        this.profile_image_url = profile_image_url;
    }

    public String getProfile_image_url_https() {
        return profile_image_url_https;
    }

    public void setProfile_image_url_https(String profile_image_url_https) {
        this.profile_image_url_https = profile_image_url_https;
    }

    public String getProfile_interstitial_type() {
        return profile_interstitial_type;
    }

    public void setProfile_interstitial_type(String profile_interstitial_type) {
        this.profile_interstitial_type = profile_interstitial_type;
    }

    public String getProfile_link_color() {
        return profile_link_color;
    }

    public void setProfile_link_color(String profile_link_color) {
        this.profile_link_color = profile_link_color;
    }

    public String getProfile_sidebar_border_color() {
        return profile_sidebar_border_color;
    }

    public void setProfile_sidebar_border_color(String profile_sidebar_border_color) {
        this.profile_sidebar_border_color = profile_sidebar_border_color;
    }

    public String getProfile_sidebar_fill_color() {
        return profile_sidebar_fill_color;
    }

    public void setProfile_sidebar_fill_color(String profile_sidebar_fill_color) {
        this.profile_sidebar_fill_color = profile_sidebar_fill_color;
    }

    public String getProfile_text_color() {
        return profile_text_color;
    }

    public void setProfile_text_color(String profile_text_color) {
        this.profile_text_color = profile_text_color;
    }

    public boolean isProfile_use_background_image() {
        return profile_use_background_image;
    }

    public void setProfile_use_background_image(boolean profile_use_background_image) {
        this.profile_use_background_image = profile_use_background_image;
    }

    public boolean isProtectedX() {
        return protectedX;
    }

    public void setProtectedX(boolean protectedX) {
        this.protectedX = protectedX;
    }

    public boolean isRequire_some_consent() {
        return require_some_consent;
    }

    public void setRequire_some_consent(boolean require_some_consent) {
        this.require_some_consent = require_some_consent;
    }

    public String getScreen_name() {
        return screen_name;
    }

    public void setScreen_name(String screen_name) {
        this.screen_name = screen_name;
    }

    public int getStatuses_count() {
        return statuses_count;
    }

    public void setStatuses_count(int statuses_count) {
        this.statuses_count = statuses_count;
    }

    public String getTranslator_type() {
        return translator_type;
    }

    public void setTranslator_type(String translator_type) {
        this.translator_type = translator_type;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public boolean isWant_retweets() {
        return want_retweets;
    }

    public void setWant_retweets(boolean want_retweets) {
        this.want_retweets = want_retweets;
    }

    public List<?> getAdvertiser_account_service_levels() {
        return advertiser_account_service_levels;
    }

    public void setAdvertiser_account_service_levels(List<?> advertiser_account_service_levels) {
        this.advertiser_account_service_levels = advertiser_account_service_levels;
    }

    public List<?> getPinned_tweet_ids() {
        return pinned_tweet_ids;
    }

    public void setPinned_tweet_ids(List<?> pinned_tweet_ids) {
        this.pinned_tweet_ids = pinned_tweet_ids;
    }

    public List<?> getPinned_tweet_ids_str() {
        return pinned_tweet_ids_str;
    }

    public void setPinned_tweet_ids_str(List<?> pinned_tweet_ids_str) {
        this.pinned_tweet_ids_str = pinned_tweet_ids_str;
    }

    public static class EntitiesBean {
        /**
         * description : {"urls":[]}
         */

        private DescriptionBean description;

        public DescriptionBean getDescription() {
            return description;
        }

        public void setDescription(DescriptionBean description) {
            this.description = description;
        }

        public static class DescriptionBean {
            private List<?> urls;

            public List<?> getUrls() {
                return urls;
            }

            public void setUrls(List<?> urls) {
                this.urls = urls;
            }
        }
    }
}
