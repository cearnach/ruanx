package com.ruanx.twitter.entity;

import com.ruanx.twitter.enumeration.IgnoreTypeEnum;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import java.util.Date;

/**
 * <p>标题: </p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2019</p>
 * <p>公司: 智业软件股份有限公司</p>
 *
 * @version: 1.0
 * @author: ruansheng
 * @date 5/13/2019
 */
@Entity(name = "twitter_ignore_item")
@Data
@DynamicInsert
@DynamicUpdate
@NoArgsConstructor
@Accessors(chain = true)
public class IgnoreTwitterItem {

    @EmbeddedId
    private IgnoreTwitterItemId id;

    /**
     * 标识
     */
    private String flag;

    /**
     * 更新时间
     */
    @Column(columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP")
    private Date updateTime;

    public IgnoreTwitterItem(String id) {
        this.id = new IgnoreTwitterItemId().setItem(id).setType(IgnoreTypeEnum.TWITTER_ACCOUNT.getCode());
    }
}
