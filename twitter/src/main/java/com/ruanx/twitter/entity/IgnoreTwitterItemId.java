package com.ruanx.twitter.entity;


import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

@Data
@Embeddable
@Accessors(chain = true)
public class IgnoreTwitterItemId implements Serializable {

    @NotEmpty
    private String item;

    @NotEmpty
    private Integer type;
}
