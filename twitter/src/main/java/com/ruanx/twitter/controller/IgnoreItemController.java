package com.ruanx.twitter.controller;

import com.ruanx.twitter.entity.IgnoreTwitterItem;
import com.ruanx.twitter.service.IgnoreItemService;
import com.google.common.hash.BloomFilter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>标题: </p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2019</p>
 * <p>公司: 智业软件股份有限公司</p>
 *
 * @version: 1.0
 * @author: ruansheng
 * @date 2019-07-05
 */
@RestController
@RequestMapping("/twitter/item")
@Slf4j
public class IgnoreItemController {

    private final IgnoreItemService ignoreItemService;
    private final BloomFilter<String> bloomFilter;
    private final List<String> cacheList;

    public IgnoreItemController(IgnoreItemService ignoreItemService,@Qualifier("mewu-bloom-filter") BloomFilter<String> bloomFilter) {
        this.ignoreItemService = ignoreItemService;
        this.bloomFilter = bloomFilter;
        cacheList = ignoreItemService.findAll()
                .stream()
                .map(ignoreTwitterItem -> ignoreTwitterItem.getId().getItem())
                .collect(Collectors.toList());
    }

    @GetMapping("/exists/{id}")
    public boolean exists(@PathVariable String id) {
        return bloomFilter.mightContain(id);
    }

    @GetMapping("/all")
    public List<String> all() {
        return cacheList;
    }

    @PutMapping("/ignore/{id}")
    public boolean ignore(@PathVariable String id) {
        IgnoreTwitterItem item = ignoreItemService.save(new IgnoreTwitterItem(id));
        this.ignoreItemService.save(item);
        bloomFilter.put(id);
        cacheList.add(id);
        log.info("添加排除ID:{}", id);
        return true;
    }

}
