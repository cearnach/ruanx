package com.ruanx.mewu.spider;

import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpResponse;
import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ruanx.mewu.entity.DataPoint;
import com.ruanx.mewu.entity.ProxyLog;
import com.ruanx.mewu.repository.MewuLogRepository;
import com.ruanx.mewu.repository.SystemParamRepository;
import com.ruanx.mewu.service.ProxyLoginService;
import com.ruanx.mewu.utils.ResourceUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.net.HttpCookie;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>标题: </p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2019</p>
 * <p>公司: 智业软件股份有限公司</p>
 *
 * @version: 1.0
 * @author: ruansheng
 * @date: 2019-12-27
 */
@Component
@Slf4j
@RequiredArgsConstructor
public class MewuLogSpider {

    private static final List<String> DOMAIN_LIST;

    private final MewuLogRepository mewuLogRepository;
    private final SystemParamRepository systemParamRepository;
    private final ProxyLoginService proxyLoginService;

    static {
        try {
            String typeStr = ResourceUtils.getResourceAsString("/domain/type.json");
            DOMAIN_LIST = JSON.parseArray(typeStr, String.class);
        } catch (IOException e) {
            throw new RuntimeException("加载域名类别列表失败");
        }
    }

    public void fetch(String type) {
        long logStartSize = this.mewuLogRepository.count();
        log.info("开始获取【{}】日志信息，当前日志记录数：{}", type, logStartSize);
        HttpCookie[] cookies = this.proxyLoginService.getCookie(type);
        HttpResponse resp = this.fetchData(type, cookies);
        List<DataPoint> dataPoints = this.parseData(resp.body());
        log.info("解析日志成功，日志数：{}，开始持久化...", dataPoints.size());
        if (CollectionUtils.isEmpty(dataPoints)) {
            log.info("日志数量为空，结束请求");
            return;
        }
        List<ProxyLog> proxyLogs = dataPoints.stream()
                .map(dataPoint -> new ProxyLog().setDataPoint(dataPoint).setType(type))
                .collect(Collectors.toList());
        mewuLogRepository.saveAll(proxyLogs);
        long logEndSize = this.mewuLogRepository.count();
        log.info("持久化【{}】成功，本次更新日志数量：{}，当前日志记录数：{}", type, logEndSize - logStartSize, logEndSize);
    }


    private HttpResponse fetchData(String type, HttpCookie[] cookies) {
        log.info("开始获取日志记录");
        String domain = systemParamRepository.getByParamKey("domain_" + type);
        String logUri = systemParamRepository.getByParamKey("log_uri");
        String logUrl = domain + logUri;
        return HttpUtil.createGet(logUrl)
                .cookie(cookies)
                .execute();
    }

    private List<DataPoint> parseData(String html) {
        log.info("开始解析日志");
        Document doc = Jsoup.parse(html);
        String dataHtml = doc.select(".card-inner").html();
        String text = StrUtil.subBetween(dataHtml, "CanvasJS.Chart(\"log_chart\",", ");");
        JSONObject json = JSON.parseObject(text);
        return json.getJSONArray("data").stream()
                .map(o -> JSON.parseObject(JSON.toJSONString(o)))
                .flatMap(jsonObj -> jsonObj.getJSONArray("dataPoints").toJavaList(DataPoint.class).stream())
                .collect(Collectors.toList());
    }

    public void fetchAll() {
        log.info("要获取的日志域名列表：{}", DOMAIN_LIST);
        for (String type : DOMAIN_LIST) {
            try {
                this.fetch(type);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
