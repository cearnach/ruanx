package com.ruanx.mewu.flater;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;

import java.util.Calendar;

/**
 * <p>标题: </p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2020</p>
 * <p>公司: 智业软件股份有限公司</p>
 *
 * @version: 1.0
 * @author: ruansheng
 * @date: 2020-01-03
 */
public abstract class AbstractTimePartitioner implements TimePartitioner {

    protected abstract Long doPartition(DateTime time, Integer timeSpan);

    protected abstract String timeFormat();

    protected Long calcTime(DateTime dateTime, Integer unit, Integer currTime, Integer timeSpan) {
        Calendar calendar = dateTime.toCalendar();
        if (timeSpan > 1) {
            int diff = currTime % timeSpan;
            if (diff > 0) {
                calendar.add(unit, -diff);
            }
        }
        String format = DateUtil.format(calendar.getTime(), this.timeFormat());
        return DateTime.of(format, this.timeFormat()).getTime();
    }

    @Override
    public Long partition(Long time, Integer timeSpan) {
        if (timeSpan == null || timeSpan < 1) {
            timeSpan = 1;
        }
        return this.doPartition(DateTime.of(time), timeSpan);
    }
}
