package com.ruanx.mewu.flater;

/**
 * <p>标题: </p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2020</p>
 * <p>公司: 智业软件股份有限公司</p>
 *
 * @version: 1.0
 * @author: ruansheng
 * @date: 2020-01-03
 */
public interface TimePartitioner {

    String BEAN_PREFIX = "TIME_PARTITIONER:";

    Long partition(Long time, Integer timeSpan);

}
