package com.ruanx.mewu.flater.impl;

import cn.hutool.core.date.DateTime;
import com.ruanx.mewu.flater.AbstractTimePartitioner;
import com.ruanx.mewu.flater.TimePartitioner;
import org.springframework.stereotype.Component;

import java.util.Calendar;

/**
 * <p>标题: </p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2020</p>
 * <p>公司: 智业软件股份有限公司</p>
 *
 * @version: 1.0
 * @author: ruansheng
 * @date: 2020-01-03
 */
@Component(TimePartitioner.BEAN_PREFIX + "DAY")
public class DayTimePartitioner extends AbstractTimePartitioner {

    @Override
    protected Long doPartition(DateTime time, Integer timeSpan) {
        return this.calcTime(time, Calendar.DAY_OF_MONTH, time.dayOfMonth(), timeSpan);
    }

    @Override
    protected String timeFormat() {
        return "yyyy-MM-dd";
    }
}
