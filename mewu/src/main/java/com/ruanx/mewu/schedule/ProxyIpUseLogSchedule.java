package com.ruanx.mewu.schedule;

import com.ruanx.mewu.service.ProxyIpUseLogService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * <p>标题: </p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2020</p>
 * <p>公司: 宜车时代信息技术有限公司</p>
 *
 * @version: 1.0
 * @author: ruansheng
 * @date: 2020-05-10
 */
@Component
@Slf4j
@RequiredArgsConstructor
public class ProxyIpUseLogSchedule {

    private final ProxyIpUseLogService proxyIpUseLogService;

    /**
     * 4分钟执行一次最近IP获取任务
     */
    @Scheduled(fixedDelay = 4 * 60 * 1000)
    public void doSchedule() {
        proxyIpUseLogService.fetchAllIpUseLog();
    }


}
