package com.ruanx.mewu.schedule;

import com.ruanx.mewu.spider.MewuLogSpider;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * <p>标题: </p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2019</p>
 * <p>公司: 智业软件股份有限公司</p>
 *
 * @version: 1.0
 * @author: ruansheng
 * @date: 2019-12-27
 */
@Component
@Slf4j
public class DataSchedule {

    private final MewuLogSpider mewuLogSpider;

    public DataSchedule(MewuLogSpider mewuLogSpider) {
        this.mewuLogSpider = mewuLogSpider;
    }

    /**
     * 获取日志
     * 每天凌晨6点执行：0 0 6 * * ?
     */
    @Scheduled(cron = "0 0 6 * * ?")
    public void doSchedule() {
        mewuLogSpider.fetchAll();
    }


}
