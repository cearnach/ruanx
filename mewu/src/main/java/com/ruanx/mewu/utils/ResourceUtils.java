package com.ruanx.mewu.utils;

import lombok.Cleanup;
import org.springframework.core.io.ClassPathResource;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * <p>标题: </p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2019</p>
 * <p>公司: 智业软件股份有限公司</p>
 *
 * @version: 1.0
 * @author: ruansheng
 * @date 2019-07-15
 */
public class ResourceUtils {

    public static String getResourceAsString(String path) throws IOException {
        //这里不能使用resource.getFile(), 当项目打包成Jar包时,会找不到文件
        ClassPathResource resource = new ClassPathResource(path);
        @Cleanup InputStream in = resource.getInputStream();
        @Cleanup InputStreamReader isr = new InputStreamReader(in);
        @Cleanup BufferedReader br = new BufferedReader(isr);
        String line;
        StringBuilder sb = new StringBuilder();
        while ((line = br.readLine()) != null) {
            sb.append(line);
        }
        return sb.toString();
    }
}
