package com.ruanx.mewu.repository;

import com.ruanx.mewu.entity.ProxyIpUseLog;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * <p>标题: </p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2020</p>
 * <p>公司: 宜车时代信息技术有限公司</p>
 *
 * @version: 1.0
 * @author: ruansheng
 * @date: 2020-05-10
 */
public interface ProxyIpUseLogRepository extends JpaRepository<ProxyIpUseLog, Integer> {
}
