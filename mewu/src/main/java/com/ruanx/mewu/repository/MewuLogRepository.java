package com.ruanx.mewu.repository;

import com.ruanx.mewu.entity.DataPoint;
import com.ruanx.mewu.entity.ProxyLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * <p>标题: </p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2019</p>
 * <p>公司: 智业软件股份有限公司</p>
 *
 * @version: 1.0
 * @author: ruansheng
 * @date: 2019-12-27
 */
public interface MewuLogRepository extends JpaRepository<ProxyLog, DataPoint> {


    @Query(value = "SELECT * FROM proxy_log WHERE type in (:types) ORDER BY y DESC LIMIT :size", nativeQuery = true)
    List<ProxyLog> findTopByY(List<String> types, Integer size);

    @Query(value = "SELECT * FROM proxy_log WHERE type in (:types) AND x >= :startDate AND x <= :endDate ORDER BY y DESC LIMIT :size", nativeQuery = true)
    List<ProxyLog> findByDate(List<String> types, Long startDate, Long endDate, Integer size);
}
