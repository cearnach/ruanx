package com.ruanx.mewu.repository;

import com.ruanx.mewu.entity.SystemParam;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * <p>标题: </p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2019</p>
 * <p>公司: 智业软件股份有限公司</p>
 *
 * @version: 1.0
 * @author: ruansheng
 * @date: 2019-12-27
 */
public interface SystemParamRepository extends JpaRepository<SystemParam, Integer> {
    @Query(value = "select param_value from mewu_system_param where param_key = :email",nativeQuery = true)
    String getByParamKey(String email);
}
