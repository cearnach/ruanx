package com.ruanx.mewu.converter;

import org.springframework.format.Formatter;

import java.text.ParseException;
import java.util.Date;
import java.util.Locale;


/**
 * <p>标题: </p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2019</p>
 * <p>公司: 智业软件股份有限公司</p>
 *
 * @version: 1.0
 * @author: ruansheng
 * @date: 2019-09-01
 */
public class CustomDateFormatter implements Formatter<Date> {
    @Override
    public Date parse(String text, Locale locale) throws ParseException {
        if ("".equals(text) || "null".equals(text)) {
            return null;
        }
        return DateConverterUtils.convert(text);
    }

    @Override
    public String print(Date object, Locale locale) {
        return String.valueOf(object.getTime());
    }
}
