package com.ruanx.mewu.controller;

import com.ruanx.mewu.repository.ProxyIpUseLogRepository;
import com.ruanx.mewu.service.ProxyIpUseLogService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>标题: </p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2020</p>
 * <p>公司: 宜车时代信息技术有限公司</p>
 *
 * @version: 1.0
 * @author: ruansheng
 * @date: 2020-05-10
 */

@RestController
@RequestMapping("/ip")
@RequiredArgsConstructor
public class IpUseLogController {

    private final ProxyIpUseLogRepository proxyIpUseLogRepository;
    private final ProxyIpUseLogService proxyIpUseLogService;

    @GetMapping("/fetch")
    public void fetch() {
        this.proxyIpUseLogService.fetchAllIpUseLog();
    }

}
