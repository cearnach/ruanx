package com.ruanx.mewu.controller;

import cn.hutool.core.collection.CollectionUtil;
import com.ruanx.mewu.entity.DataPoint;
import com.ruanx.mewu.entity.ProxyLog;
import com.ruanx.mewu.flater.TimePartitioner;
import com.ruanx.mewu.repository.MewuLogRepository;
import com.ruanx.mewu.spider.MewuLogSpider;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.stream.Collectors;

/**
 * <p>标题: </p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2019</p>
 * <p>公司: 智业软件股份有限公司</p>
 *
 * @version: 1.0
 * @author: ruansheng
 * @date: 2019-12-27
 */
@RestController
@RequestMapping("/mewu/data")
@Slf4j
public class DataController {

    private final MewuLogSpider mewuLogSpider;
    private final MewuLogRepository mewuLogRepository;
    private final Map<String, TimePartitioner> timeFlatterMap;

    public DataController(MewuLogSpider mewuLogSpider, MewuLogRepository mewuLogRepository, Map<String, TimePartitioner> timeFlatterMap) {
        this.mewuLogSpider = mewuLogSpider;
        this.mewuLogRepository = mewuLogRepository;
        this.timeFlatterMap = timeFlatterMap;
    }


    @GetMapping("/fetch")
    public String fetch() {
        mewuLogSpider.fetchAll();
        return "SUCCESS";
    }

    @GetMapping("/all")
    public List<DataPoint> all(@RequestParam List<String> types,
                               @RequestParam Date startDate,
                               @RequestParam Date endDate,
                               @RequestParam(defaultValue = "100") Integer size) {

        return mewuLogRepository.findByDate(types, startDate.getTime(), endDate.getTime(), size)
                .stream()
                .map(ProxyLog::getDataPoint)
                .collect(Collectors.toList());
    }

    @GetMapping("/top/all")
    public List<DataPoint> topAll(@RequestParam List<String> types, @RequestParam(defaultValue = "100") Integer size) {
        return mewuLogRepository.findTopByY(types, size)
                .stream()
                .map(ProxyLog::getDataPoint)
                .collect(Collectors.toList());
    }

    @GetMapping("/partition/{strategy}")
    public List<DataPoint> partitionByHour(@RequestParam List<String> types,
                                           @RequestParam Date startDate,
                                           @RequestParam Date endDate,
                                           @RequestParam(defaultValue = "100") Integer size,
                                           @RequestParam(defaultValue = "1") Integer timeSpan,
                                           @RequestParam(defaultValue = "true") boolean group,
                                           @PathVariable String strategy) {
        TimePartitioner timePartitioner = this.timeFlatterMap.get(TimePartitioner.BEAN_PREFIX.concat(strategy));
        if (timePartitioner == null) {
            throw new RuntimeException("找不到对应的时间分区器");
        }
        // 先根据时间（天|小时|分钟）跨度分组
        Map<Long, List<DataPoint>> timeGroup = mewuLogRepository.findByDate(types, startDate.getTime(), endDate.getTime(), size)
                .stream()
                .map(proxyLog -> {
                    DataPoint dataPoint = proxyLog.getDataPoint();
                    Long partitionX = timePartitioner.partition(dataPoint.getX(), timeSpan);
                    dataPoint.setX(partitionX);
                    return dataPoint;
                })
                .collect(Collectors.groupingBy(DataPoint::getX));

        if (group) {
            // 对每个时区里面的节点根据节点名称进行分组，再对分组后的集合进行统计
            return timeGroup.entrySet().stream()
                    .flatMap(e -> {
                        Long timeKey = e.getKey();
                        List<DataPoint> values = e.getValue();
                        // 对节点名称进行分组
                        Map<String, List<DataPoint>> nameGroup = values.stream().collect(Collectors.groupingBy(DataPoint::getJd));
                        return nameGroup.entrySet().stream()
                                .map(et -> {
                                    double sumY = et.getValue().stream().mapToDouble(DataPoint::getY).sum();
                                    return new DataPoint().setX(timeKey).setJd(et.getKey()).setY(sumY);
                                }).collect(Collectors.toList()).stream();
                    }).collect(Collectors.toList());

        }
        return timeGroup.entrySet().stream()
                .map(e -> {
                    Long timeKey = e.getKey();
                    List<DataPoint> values = e.getValue();
                    List<DataPoint> dataPoints = values.stream()
                            .sorted((o1, o2) -> (int) (o2.getY() - o1.getY()))
                            .collect(Collectors.toList());
                    List<DataPoint> subPoints = CollectionUtil.sub(dataPoints, 0, 3);
                    String pointName = subPoints.stream()
                            .collect(Collectors.collectingAndThen(
                                    Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(DataPoint::getJd))), ArrayList::new)
                            ).stream()
                            .map(DataPoint::getJd).reduce((s, s2) -> s + "\n" + s2).orElse("");
                    double sum = values.stream().mapToDouble(DataPoint::getY).sum();
                    return new DataPoint().setX(timeKey).setJd(pointName).setY(sum);
                }).collect(Collectors.toList());

    }

}
