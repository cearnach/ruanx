package com.ruanx.mewu.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * <p>标题: </p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2019</p>
 * <p>公司: 智业软件股份有限公司</p>
 *
 * @version: 1.0
 * @author: ruansheng
 * @date: 2019-12-27
 */
@Data
@Embeddable
@Accessors(chain = true)
public class DataPoint implements Serializable {

    /**
     * x : 1577414990000
     * y : 7.22
     * jd : V4-香港-CN2 - 三网推荐
     */
    private Long x;
    private Double y;
    private String jd;

}
