package com.ruanx.mewu.service;

import cn.hutool.http.HttpResponse;
import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSON;
import com.ruanx.mewu.entity.ProxyIpUseLog;
import com.ruanx.mewu.repository.ProxyIpUseLogRepository;
import com.ruanx.mewu.repository.SystemParamRepository;
import com.ruanx.mewu.utils.ResourceUtils;
import com.ruanx.mewu.utils.StringUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.net.HttpCookie;
import java.util.List;

/**
 * <p>标题: </p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2020</p>
 * <p>公司: 宜车时代信息技术有限公司</p>
 *
 * @version: 1.0
 * @author: ruansheng
 * @date: 2020-05-10
 */
@Component
@RequiredArgsConstructor
@Slf4j
public class ProxyIpUseLogService {

    private static final List<String> DOMAIN_LIST;
    private final ProxyLoginService proxyLoginService;
    private final ProxyIpUseLogRepository proxyIpUseLogRepository;
    private final SystemParamRepository systemParamRepository;

    static {
        try {
            String typeStr = ResourceUtils.getResourceAsString("/domain/type.json");
            DOMAIN_LIST = JSON.parseArray(typeStr, String.class);
        } catch (IOException e) {
            throw new RuntimeException("加载域名类别列表失败");
        }
    }

    public void fetchAllIpUseLog() {

        for (String type : DOMAIN_LIST) {
            try {
                log.info("开始获取代理最近使用的IP记录");
                String domain = systemParamRepository.getByParamKey("domain_" + type);
                String uri = systemParamRepository.getByParamKey("ip_use_log");
                HttpCookie[] cookies = proxyLoginService.getCookie(type);
                HttpResponse result = HttpUtil.createGet(domain + uri)
                        .cookie(cookies)
                        .execute();
                Document doc = Jsoup.parse(result.body());
                Elements cards = doc.select(".card-main");
                if (CollectionUtils.isEmpty(cards)) {
                    continue;
                }
                Elements tds = cards.get(1).select("td");
                if (CollectionUtils.isEmpty(tds)) {
                    continue;
                }
                int updateCount = 0;
                for (int i = 0; i < tds.size(); i += 2) {
                    Element ipEl = tds.get(i);
                    Element areaEl = tds.get(i + 1);
                    String ip = ipEl.text();
                    String area = areaEl.text();
                    if (StringUtils.isEmpty(area) || area.contains("局域网") || area.contains("本机")) {
                        continue;
                    }
                    ProxyIpUseLog proxyIpUseLog = new ProxyIpUseLog()
                            .setIp(ip)
                            .setArea(area)
                            .setType(type);
                    proxyIpUseLogRepository.save(proxyIpUseLog);
                    log.info("新增最近使用的IP记录, {} : {}", area, ip);
                    updateCount++;
                }
                log.info("更新最近使用的IP记录成功，共新增{}条记录", updateCount);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
