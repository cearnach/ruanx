package com.ruanx.mewu.service;

import cn.hutool.http.HttpResponse;
import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ruanx.mewu.repository.SystemParamRepository;
import com.ruanx.mewu.utils.StringUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.net.HttpCookie;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * <p>标题: </p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2020</p>
 * <p>公司: 宜车时代信息技术有限公司</p>
 *
 * @version: 1.0
 * @author: ruansheng
 * @date: 2020-05-10
 */
@Component
@RequiredArgsConstructor
@Slf4j
public class ProxyLoginService {

    private final SystemParamRepository systemParamRepository;
    private static final Map<String, HttpCookie[]> COOKIE_MAP = new ConcurrentHashMap<>(2);
    private static Map<String, LocalDateTime> LAST_LOGIN_TIME_MAP = new ConcurrentHashMap<>(2);


    private HttpCookie[] login(String type) {
        String email = systemParamRepository.getByParamKey("email");
        String password = systemParamRepository.getByParamKey("password");
        String domain = systemParamRepository.getByParamKey("domain_" + type);
        String loginUri = systemParamRepository.getByParamKey("login_uri");
        String loginUrl = domain + loginUri;
        log.info("登录用户, 用户名：{} , 密码：{} , 登录地址：{}", email, password, loginUrl);
        HttpResponse resp = HttpUtil.createPost(loginUrl)
                .form("email", email)
                .form("passwd", password)
                .execute();
        String respBody = resp.body();
        JSONObject json = JSON.parseObject(respBody);
        if (StringUtils.isEmpty(respBody) || !"1".equals(json.getString("ret"))) {
            log.error("登录失败：{}", respBody);
            throw new RuntimeException("登录失败");
        }
        List<HttpCookie> cookies = resp.getCookies();
        log.info("登录成功，Cookies：{}", cookies);
        HttpCookie[] cookieArray = cookies.toArray(new HttpCookie[0]);
        COOKIE_MAP.put(type, cookieArray);
        LAST_LOGIN_TIME_MAP.put(type, LocalDateTime.now());
        return cookieArray;
    }

    public HttpCookie[] getCookie(String type) {
        LocalDateTime lastLoginTime = LAST_LOGIN_TIME_MAP.get(type);
        if (lastLoginTime == null || lastLoginTime.plusHours(20).isBefore(LocalDateTime.now())) {
            return this.login(type);
        }
        log.info("从缓存中获取COOKIE【{}】", type);
        // 不能使用getOrDefault(),会自动调用后面的方法
        HttpCookie[] httpCookies = COOKIE_MAP.get(type);
        return httpCookies == null ? this.login(type) : httpCookies;
    }

}
