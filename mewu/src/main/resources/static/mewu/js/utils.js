//获取最近7天日期

getDay(0)//当天日期

getDay(-7)//7天前日期

//获取最近3天日期

getDay(0)//当天日期

getDay(-3)//3天前日期

function getDay (day) {

  var today = new Date()

  var targetday_milliseconds = today.getTime() + 1000 * 60 * 60 * 24 * day

  today.setTime(targetday_milliseconds) //注意，这行是关键代码

  var tYear = today.getFullYear()

  var tMonth = today.getMonth()

  var tDate = today.getDate()

  tMonth = doHandleMonth(tMonth + 1)

  tDate = doHandleMonth(tDate)

  return tYear + '-' + tMonth + '-' + tDate

}

function doHandleMonth (month) {

  var m = month

  if (month.toString().length == 1) {

    m = '0' + month

  }

  return m

}

function fetch (args) {
  $.get('/mewu/data/fetch').then(res => {
      alert(res)
    }
  )
}

function query (myChart) {
  const startDate = $('#startDate').val()
  const endDate = $('#endDate').val()
  const size = $('#size').val()
  const strategy = $('#timeSpanType').val().toUpperCase()
  const timeSpan = $('#timeSpan').val()
  const groupType = $('#chk-groupByPointName').get(0).checked
  let types = $('#logType').val()
  if (types === 'all') {
    types = ''
    const typeOptions = $('#logType option')
    for (let i = 0; i < typeOptions.length; i++) {
      let optVal = typeOptions[i].value
      if (optVal !== 'all') {
        types += '&types=' + optVal
      }
    }
  } else {
    types = '&types=' + types
  }
  let url = '/mewu/data/partition/' + strategy + '?startDate=' + startDate + '&endDate=' + endDate
    + '&size=' + size + '&timeSpan=' + timeSpan + '&group=' + groupType + types
  $.get(url).then(res => {
    let logs = []
    let minTime = -1
    let minY = -1
    for (const dataPoint of res) {
      dataPoint.y = parseInt(dataPoint.y / 1024)
      if (dataPoint.x < minTime || minTime === -1) {minTime = dataPoint.x}
      if (dataPoint.y < minY || minY === -1) {minY = dataPoint.y}
      logs.push([dataPoint.x, dataPoint.y, dataPoint.jd])
    }

    const option = {
      xAxis: {
        min: minTime,
        axisLabel: {
          formatter: function (val) {
            return new Date(parseInt(val)).Format("yyyy-MM-dd hh:mm:ss")
          }
        }
      },
      yAxis: {
        min: minY,
        axisLabel: {
          formatter: function (val) {
            return val + ' MB'
          }
        }
      },
      series: [{
        label: {
          backgroundColor: 'white',
          emphasis: {
            show: true,
            formatter: function (param) {
              // [x,y,jd]
              const size = param.data[1] + ' MB\n\n'
              const point = param.data[2] + '\n\n'
              const date = new Date(parseInt(param.data[0])).Format("yyyy-MM-dd hh:mm:ss")
              return size + point + date
            },
            position: 'top'
          }
        },
        symbolSize: 10,
        data: logs,
        type: 'scatter'
      }]
    }
    // 使用刚指定的配置项和数据显示图表。
    myChart.setOption(option)
  })
}


// 对Date的扩展，将 Date 转化为指定格式的String
// 月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符，
// 年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字)
// 例子：
// (new Date()).Format("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423
// (new Date()).Format("yyyy-M-d h:m:s.S")      ==> 2006-7-2 8:9:4.18
Date.prototype.Format = function(fmt)
{ //author: meizz
  var o = {
    "M+" : this.getMonth()+1,                 //月份
    "d+" : this.getDate(),                    //日
    "h+" : this.getHours(),                   //小时
    "m+" : this.getMinutes(),                 //分
    "s+" : this.getSeconds(),                 //秒
    "q+" : Math.floor((this.getMonth()+3)/3), //季度
    "S"  : this.getMilliseconds()             //毫秒
  };
  if(/(y+)/.test(fmt))
    fmt=fmt.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length));
  for(var k in o)
    if(new RegExp("("+ k +")").test(fmt))
      fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));
  return fmt;
}
