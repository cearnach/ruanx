FROM openjdk:8-jdk-alpine
VOLUME /tmp
COPY ruanx-web/target/ruanx.jar ruanx.jar
EXPOSE 35000
EXPOSE 5005
ENTRYPOINT ["java","-jar","/ruanx.jar","--spring.profiles.active=deploy"]
#ENTRYPOINT ["java","-jar", "-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=5005", "/ne.jar","--spring.profiles.active=deploy"]
